<?php
/*This file is part of child-theme, prime child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/

function child_theme_enqueue_child_styles() {
$parent_style = 'parent-style';
	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style(
		'child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_style ),
		wp_get_theme()->get('Version') );
	}
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_child_styles' );

// Custom shortcodes for quizes

// Create shortcode for slider on home page
function wpk_main_slider($atts)
{

    $args = array(
        'post_type' => 'wp_quiz',
		'posts_per_page' => 4,
		'meta_query' => array(
			array(
				'key' => 'featured_quiz',
				'compare' => '=',
				'value' => '1'
			)
		)
    );

	$slider_posts = new WP_Query($args);


	echo '<div id="wpk-slick-slider">';
	while ($slider_posts->have_posts()): $slider_posts->the_post();
		$categories = get_the_category();
		//$categories_id = get_cat_ID( current($categories)->name );
		echo '<div class="wpk-slick-slider_image">';
		echo get_the_post_thumbnail();
		echo '<div class="wpk-slick-slider_content">';
			echo '<span class="wpk-slick-slider_category">';
			echo current($categories)->name;
			echo '</span>';
			echo '<p class="wpk-slick-slider_title">';
				the_title();
			echo '</p>';
			 the_excerpt();
		echo '</div></div>';



    endwhile;
    echo '</div>';
    wp_reset_postdata();

}
add_shortcode('wpk_slider', 'wpk_main_slider');

function wpk_show_quiz($atts) {

	$a = shortcode_atts( array(
		'category' => ' '
	), $atts );

	$args = array(
		'post_type' => 'wp_quiz',
		'category_name' => $a['category'],
		'posts_per_page' => 6
	);

	$quiz_posts = new WP_Query($args);
	echo '<div id="wpk-quiz-container">';
	while ( $quiz_posts->have_posts() ): $quiz_posts->the_post();
		$categories = get_the_category();
		echo '<div id="wpk-quiz-list">';
		echo get_the_post_thumbnail();
		// echo '</div>';
		echo '<div class="wpk-quiz-list_content">';
			echo '<span class="wpk-quiz-list_category">';
			echo current($categories)->name;
			echo '</span>';
			echo '<p><a href="' . get_the_permalink() .  '" class="wpk-quiz-list_title">';
				the_title();
			echo '</a></p>';

		echo '</div></div>';

	endwhile;
	echo '</div>';
    wp_reset_postdata();

}
add_shortcode('wpk_quiz', 'wpk_show_quiz');