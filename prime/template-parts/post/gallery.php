<?php
/*----------------------------------------------------------------------------*/
/*--[ POST: GALLERY ]--*/
/*----------------------------------------------------------------------------*/

$images  = mpcth_get_metabox( '_mpcth_gallery_images', '' );
$gallery = '';

if ( ! empty( $images ) ) {
	$images_list = '';
	$placeholder = '';
	$nav_id      = uniqid( 'mpcth_nav-' );
	$max_height  = 0;

	foreach ( $images as $image ) {
		if ( isset( $image[ 'sizes' ][ 'mpcth-thumb--large' ] ) ) {
			$image_src = $image[ 'sizes' ][ 'mpcth-thumb--large' ];
		} else {
			$image_src = $image[ 'url' ];
		}

		$images_list .= '<div style="background-image:url(' . $image_src . ');"></div>';

		if ( isset( $image[ 'sizes' ][ 'mpcth-thumb--large' ] ) ) {
			if ( $image[ 'sizes' ][ 'mpcth-thumb--large-height' ] > $max_height ) {
				$max_height  = $image[ 'sizes' ][ 'mpcth-thumb--large-height' ];
				$placeholder = $image_src;
			}
		} else {
			if ( $image[ 'height' ] > $max_height ) {
				$max_height  = $image[ 'height' ];
				$placeholder = $image_src;
			}
		}
	}

	?>

	<header class="mpcth_post__image">
		<div class="mpcth_gallery" data-nav-id="<?php echo $nav_id; ?>">
			<?php echo $images_list; ?>
		</div>
		<div class="mpcth_gallery__nav" data-nav-id="<?php echo $nav_id; ?>">
			<a href="#" class="mpcth_prev mpcth_icon mti-fa-angle-left"></a>
			<a href="#" class="mpcth_next mpcth_icon mti-fa-angle-right"></a>
		</div>
		<img class="mpcth_gallery__placeholder" src="<?php echo $placeholder; ?>">
	</header>

	<?php
}
