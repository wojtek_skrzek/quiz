<?php
/*----------------------------------------------------------------------------*/
/*--[ POST: THUMBNAIL ]--*/
/*----------------------------------------------------------------------------*/

if ( get_post_format() == 'gallery' ) {
	get_template_part( 'template-parts/post/gallery' );
} elseif ( has_post_thumbnail() ) {
	$image = get_the_post_thumbnail( get_the_ID(), 'mpcth-thumb--large', array(
		'class'	=> 'mpcth_thumbnail',
		'title'	=> get_the_title(),
		'alt'   => get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true),
	) );

	if ( $image ) : ?>
		<header class="mpcth_post__image">
			<?php echo $image; ?>
		</header>
	<?php endif;
}