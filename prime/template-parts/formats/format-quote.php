<?php
/*----------------------------------------------------------------------------*/
/*--[ FORMAT: QUOTE ]--*/
/*----------------------------------------------------------------------------*/
?>

<div class="mpcth_quote">
	<blockquote>
		<?php echo mpcth_get_metabox( '_mpcth_quote_text', '' ); ?>
		<cite><?php echo mpcth_get_metabox( '_mpcth_quote_author', '' ); ?></cite>
	</blockquote>
</div>
