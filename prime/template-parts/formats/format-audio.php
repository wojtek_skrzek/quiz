<?php
/*----------------------------------------------------------------------------*/
/*--[ FORMAT: AUDIO ]--*/
/*----------------------------------------------------------------------------*/

$audio = mpcth_get_metabox( '_mpcth_audio_embed', '' );

if ( ! $audio ) {
	$audio = mpcth_get_metabox( '_mpcth_audio_upload', '' );

	if ( $audio ) {
		$audio = do_shortcode( '[audio src=' . $audio . ']' );
	}
}

?>

<div class="mpcth_audio">
	<?php echo $audio; ?>
</div>
