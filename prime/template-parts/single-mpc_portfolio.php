<?php
/*----------------------------------------------------------------------------*/
/*--[ SINGLE PORTFOLIO ]--*/
/*----------------------------------------------------------------------------*/

$format        = get_post_format() ?: 'standard';
$disable_title = mpcth_get_metabox( '_mpcth_disable_title', false );
$tags          = get_the_term_list( get_the_ID(), 'mpc_portfolio_tag', '', ', ', '' );

?>

<article id="post_<?php the_ID(); ?>" <?php post_class( 'mpcth_post' ); ?>>
	<header class="mpcth_post__header">
		<?php
		if ( ! $disable_title ) {
			mpcth_display_breadcrumb();

			the_title( '<h1 class="mpcth_post__title">', '</h1>' );

			get_template_part( 'template-parts/portfolio/meta' );
		}

		get_template_part( 'template-parts/portfolio/thumbnail' );
		?>

		<?php if ( ! in_array( $format, array( 'standard', 'image', 'gallery' ) ) ) : ?>
			<div class="mpcth_post__format">
				<?php get_template_part( 'template-parts/formats/format', $format ); ?>
			</div>
		<?php endif; ?>
	</header>

	<div class="mpcth_post__content">
		<?php the_content(); ?>
	</div>

	<footer class="mpcth_post__footer">
		<div class="mpcth_post__nav">
			<?php
			previous_post_link( '%link', '<i class="mpcth_icon mti-fa-angle-left"></i>%title' );
			next_post_link( '%link', '%title<i class="mpcth_icon mti-fa-angle-right"></i>' );
			?>
		</div>

		<?php if ( $tags ) : ?>
			<div class="mpcth_post__tags"><?php _e( 'Tags: ', 'mpcth' ); ?><?php echo $tags; ?></div>
		<?php endif; ?>

		<?php edit_post_link( esc_html__( 'Edit', 'mpcth' ), '<div class="mpcth_signel__edit">', '</div>' ); ?>
	</footer>
</article>
