<?php
/*----------------------------------------------------------------------------*/
/*--[ NOT FOUND ]--*/
/*----------------------------------------------------------------------------*/
?>

<section class="mpcth_not-found">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

		<h4><?php __( 'Publish your first post!', 'mpcth' ); ?><a href="<?php echo esc_url( admin_url( 'post-new.php' ) ); ?>"><?php _e( 'Create new post.', 'mpcth' ); ?></a></h4>

	<?php elseif ( is_search() ) : ?>

		<h4><?php _e( 'We couldn&rsquo;t find anything matching your search terms. Please try something different :)', 'mpcth' ); ?></h4>
		<?php get_search_form(); ?>

	<?php else : ?>

		<h4><?php _e( 'Oops... this probably isn&rsquo;t what you&rsquo;re looking for. Perhaps searching can help :)', 'mpcth' ); ?></h4>
		<?php get_search_form(); ?>

	<?php endif; ?>
</section><!-- .mpcth_not-found -->
