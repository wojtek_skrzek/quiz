<?php
/*----------------------------------------------------------------------------*/
/*--[ THEME: HEADER ]--*/
/*----------------------------------------------------------------------------*/

global $mpc_theme;
$mpcth_layout = $mpc_theme[ 'header__layout' ];

$tagline = '';
if ( $mpc_theme[ 'header__tagline' ] ) {
	$tagline_text = trim( $mpc_theme[ 'header__tagline-text' ] );
	$tagline      = '<span class="mpcth_tagline">' . ( $tagline_text ? $tagline_text : get_bloginfo( 'description' ) ) . '</span>';
}

$mpcth_header_classes = ' mpcth_style--' . $mpc_theme[ 'header__style' ];
$mpcth_header_classes .= $mpc_theme[ 'header__float' ] ? ' mpcth_float' : '';
$mpcth_header_classes .= ' mpcth_background--' . $mpc_theme[ 'header__background-type' ];
$mpcth_header_classes .= ' mpcth_background-sticky--' . $mpc_theme[ 'header_sticky__background-type' ];
$mpcth_header_classes .= $mpc_theme[ 'menu__submenu-sign' ] ? ' mpcth_submenu-sign' : '';
$mpcth_header_classes .= $mpc_theme[ 'header__sticky' ] ? '' : ' mpcth_no-sticky';

$wrap_classes = '';

if ( mpcth_get_metabox( '_mpcth_enable_alt_header', false ) ) {
	$mpcth_header_classes .= ' mpcth_alt';
	$mpcth_header_classes .= ' mpcth_background-alt--' . $mpc_theme[ 'header_alt__background-type' ];
}

?>

<div id="mpcth_page_header__anchor"></div>
<header id="mpcth_page_header" class="mpcth_page__header<?php echo $mpcth_header_classes; ?>" role="banner">
	<div class="mpcth_layout--block<?php echo $wrap_classes; ?>">
		<div class="mpcth_background-target"></div>
		<div class="mpcth_style-wrap mpcth_header-layout mpcth_layout--<?php echo $mpcth_layout; ?>">

			<a id="mpcth_navigation_toggle" href="#" class="mpcth_navigation-toggle"><span class="mpcth_icon mti-fa-bars"></span></a>

			<div class="mpcth_logo-wrap">
				<h2 class="mpcth_logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo mpcth_display_logo(); ?></a></h2>
				<h2 class="mpcth_logo mpcth_mobile"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo mpcth_display_logo( 'mobile_logo' ); ?></a></h2>
				<?php echo $tagline; ?>
			</div>

			<a id="mpcth_search_toggle" href="#" class="mpcth_search-toggle"><span class="mpcth_icon mti-fa-search"></span></a>

			<?php if ( $mpcth_layout != 'layout_05' ) : ?>
				<nav id="mpcth_page_navigation" class="mpcth_page__navigation" role="navigation">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'primary',
						'container'      => '',
						'menu_id'        => 'primary-menu',
						'menu_class'     => 'mpc-menu mpcth_background--' . $mpc_theme[ 'dropdown__background-type' ],
						'walker'         => class_exists( 'MPC_Mega_Menu_Walker' ) ? new MPC_Mega_Menu_Walker() : '',
					) );
					?>
				</nav>
			<?php else : ?>
				<nav id="mpcth_page_navigation_left" class="mpcth_page__navigation mpcth_side--left" role="navigation">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'primary-left',
						'container'      => '',
						'menu_id'        => 'primary-menu-left',
						'menu_class'     => 'mpc-menu mpcth_background--' . $mpc_theme[ 'dropdown__background-type' ],
						'walker'         => class_exists( 'MPC_Mega_Menu_Walker' ) ? new MPC_Mega_Menu_Walker() : '',
					) );
					?>
				</nav>
				<nav id="mpcth_page_navigation_right" class="mpcth_page__navigation mpcth_side--right" role="navigation">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'primary-right',
						'container'      => '',
						'menu_id'        => 'primary-menu-right',
						'menu_class'     => 'mpc-menu mpcth_background--' . $mpc_theme[ 'dropdown__background-type' ],
						'walker'         => class_exists( 'MPC_Mega_Menu_Walker' ) ? new MPC_Mega_Menu_Walker() : '',
					) );
					?>
				</nav>
			<?php endif; ?>

			<?php if ( $mpcth_layout == 'layout_05' ) : ?>
				</div>
				<div class="mpcth_style-wrap mpcth_header-layout mpcth_second mpcth_layout--<?php echo $mpcth_layout; ?>">
			<?php endif; ?>

			<?php if ( ! $mpc_theme[ 'header__disable-search' ] ) : ?>
				<div id="mpcth_page_search" class="mpcth_page__search">
					<?php get_search_form(); ?>
				</div>
			<?php endif; ?>

			<?php if ( ! $mpc_theme[ 'header__disable-socials' ] && ! empty( $mpc_theme[ 'socials__list' ] ) ) : ?>
				<div id="mpcth_socials" class="mpcth_socials">
					<?php mpcth_display_socials( true ); ?>
				</div>
			<?php endif; ?>

			<?php do_action( 'mpcth/header/elements' ); ?>

			<div class="mpcth_spacer"></div>

		</div>
	</div>
</header><!-- #mpcth_page_header -->

<?php if ( $mpcth_layout == 'layout_06' ) : ?>
	<div class="mpcth_logo-wrap mpcth_static">
		<h2 class="mpcth_logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo mpcth_display_logo(); ?></a></h2>
		<h2 class="mpcth_logo mpcth_mobile"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php echo mpcth_display_logo( 'mobile_logo' ); ?></a></h2>
		<?php echo $tagline; ?>
	</div>
<?php endif; ?>