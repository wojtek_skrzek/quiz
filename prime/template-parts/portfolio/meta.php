<?php
/*----------------------------------------------------------------------------*/
/*--[ PORTFOLIO: METABOXES ]--*/
/*----------------------------------------------------------------------------*/

$categories_list = get_the_term_list( get_the_ID(), 'mpc_portfolio_cat', '', ', ', '' );

$author_id = get_the_author_meta( 'ID' );
if ( $author_id === '' ) {
	$author_id = get_the_author_meta( 'ID', get_post_field( 'post_author', get_the_ID() ) );
}

?>

<div class="mpcth_post__meta">
	<span class="mpcth_meta__date">
		<span><?php echo esc_html__( 'On', 'mpcth' ); ?></span>
		<a href="<?php echo get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ); ?>">
			<time datetime="<?php echo get_the_date( 'c' ); ?>">
				<span class="mpcth_month"><?php echo get_the_date( 'M' ); ?></span>
				<span class="mpcth_day"><?php echo get_the_date( 'd' ); ?></span>
				<span class="mpcth_year"><?php echo get_the_date( 'Y' ); ?></span>
			</time>
		</a>
	</span>

	<?php if ( $categories_list ) : ?>
		<span class="mpcth_meta__categories">
			<span><?php echo esc_html__( 'In', 'mpcth' ); ?></span>
			<?php echo $categories_list; ?>
		</span>
	<?php endif; ?>

	<span class="mpcth_meta__author">
		<span><?php echo esc_html__( 'By', 'mpcth' ); ?></span>
		<a href="<?php echo get_author_posts_url( $author_id ); ?>"><?php echo get_the_author_meta( 'display_name', $author_id ); ?></a>
	</span>
</div>
