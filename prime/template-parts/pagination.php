<?php
/*----------------------------------------------------------------------------*/
/*--[ PAGINATION ]--*/
/*----------------------------------------------------------------------------*/

/** @var string $style */

global $paged, $mpc_query, $mpc_theme;

if ( empty( $mpc_query ) ) {
	global $wp_query;
	$mpc_query = $wp_query;
}

if ( ! isset( $style ) || $style == 'classic' ) {
	$pagination = paginate_links( array(
		'base'      => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
		'current'   => max( 1, $paged ),
		'total'     => $mpc_query->max_num_pages,
		'prev_text' => apply_filters( 'ma/pagination/prev', '<i class="mpcth_icon mti-fa-angle-left"></i>' ),
		'next_text' => apply_filters( 'ma/pagination/next', '<i class="mpcth_icon mti-fa-angle-right"></i>' ),
		'end_size'  => 3,
		'mid_size'  => 3,
		'type'      => 'array'
	) );

	echo implode( '<span class="mpcth_separator"></span>', $pagination );
} else {
	$data = ' data-current="' . max( 1, $paged ) . '"';
	$data .= ' data-total="' . $mpc_query->max_num_pages . '"';
	echo '<a href="' . str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ) . '" id="mpcth_load_more" class="mpcth_load-more' . ( $style == 'infinite' ? ' mpcth_infinite' : '' ) . '" ' . $data . '>' . esc_html( $mpc_theme[ 'pagination__load-more' ] ) . '</a>';
}
