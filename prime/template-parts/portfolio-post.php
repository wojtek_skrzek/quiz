<?php
/*----------------------------------------------------------------------------*/
/*--[ PORTFOLIO POST ]--*/
/*----------------------------------------------------------------------------*/

global $mpc_theme;

$wrapper = $mpc_theme[ 'portfolio__columns' ] != '1';

$classes = '';
if ( $mpc_theme[ 'portfolio__post-color' ] && $mpc_theme[ 'portfolio__post-color' ] != 'transparent' ) {
	$classes = 'mpcth_with-background';
}

?>

<?php if ( $wrapper ) : ?>
	<div class="mpcth_grid-item">
<?php endif; ?>

<article id="portfolio_<?php the_ID(); ?>" <?php post_class( 'mpcth_post ' . $classes ); ?>>
	<div class="mpcth_post__content">
		<?php get_template_part( 'template-parts/portfolio/thumbnail' ) ?>

		<h2 class="mpcth_post__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

		<?php get_template_part( 'template-parts/portfolio/meta' ) ?>

		<div class="mpcth_post__excerpt"><?php the_excerpt(); ?></div>

		<div class="mpcth_post__read-more"><a href="<?php the_permalink(); ?>"><?php _e( 'Read more', 'mpcth' ); ?></a></div>
	</div>
</article>

<?php if ( $wrapper ) : ?>
	</div>
<?php endif; ?>
