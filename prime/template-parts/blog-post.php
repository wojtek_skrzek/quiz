<?php
/*----------------------------------------------------------------------------*/
/*--[ BLOG POST ]--*/
/*----------------------------------------------------------------------------*/

global $mpc_theme;

$format    = get_post_format() ?: 'standard';
$permalink = get_the_permalink();
$read_more = __( 'Read more', 'mpcth' );

if ( $format == 'link' ) {
	$permalink = mpcth_get_metabox( '_mpcth_link_url', $permalink );
	$read_more = __( 'Continue reading' );
}

$wrapper = $mpc_theme[ 'blog__columns' ] != '1';

$classes = '';
if ( $mpc_theme[ 'blog__post-color' ] && $mpc_theme[ 'blog__post-color' ] != 'transparent' ) {
	$classes = 'mpcth_with-background';
}

?>

<?php if ( $wrapper ) : ?>
	<div class="mpcth_grid-item">
<?php endif; ?>

<article id="post_<?php the_ID(); ?>" <?php post_class( 'mpcth_post ' . $classes ); ?>>
	<div class="mpcth_post__content">
		<?php if ( $format != 'aside' ) : ?>
			<?php get_template_part( 'template-parts/post/thumbnail' ) ?>

			<?php if ( ! in_array( $format, array( 'standard', 'image', 'gallery' ) ) ) : ?>
			<div class="mpcth_post__format">
				<?php get_template_part( 'template-parts/formats/format', $format ); ?>
			</div>
			<?php endif; ?>

			<h2 class="mpcth_post__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

			<?php get_template_part( 'template-parts/post/meta' ) ?>
		<?php endif; ?>

		<div class="mpcth_post__excerpt"><?php the_excerpt(); ?></div>

		<div class="mpcth_post__read-more"><a href="<?php echo $permalink; ?>"><?php echo $read_more; ?></a></div>
	</div>
</article>

<?php if ( $wrapper ) : ?>
	</div>
<?php endif; ?>
