<?php
/*----------------------------------------------------------------------------*/
/*--[ THEME: HEAD ]--*/
/*----------------------------------------------------------------------------*/

global $mpc_theme;
global $paged;
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 );

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="mpcth_back_to_top__anchor"></div>

<?php do_action( 'mpcth/body/begin' ); ?>

<?php if ( $mpc_theme[ 'global__display' ] == 'frame' ) : ?>
	<div id="mpcth_frame" class="mpcth_frame"></div>
<?php endif; ?>

<div id="mpcth_page" class="hfeed mpcth_page">
	<?php do_action( 'mpcth/site/begin' ); ?>

	<div id="mpcth_page_content" class="mpcth_page__content mpcth_background--<?php echo $mpc_theme[ 'global__background-type' ]; ?>">

		<?php if ( $mpc_theme[ 'header__content-switch' ] && trim( $mpc_theme[ 'header__content' ] ) != '' ) : ?>
			<div id="mpcth_pre_header" class="mpcth_pre-header mpcth_layout--block"><?php echo do_shortcode( trim( $mpc_theme[ 'header__content' ] ) ); ?></div>
		<?php endif; ?>

		<?php get_template_part( 'template-parts/header' ); ?>

		<div class="mpcth_content mpcth_layout--block clear">

			<?php do_action( 'mpcth/content/begin' ); ?>
