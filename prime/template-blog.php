<?php
/**
 * Template Name: Blog
 */

/*----------------------------------------------------------------------------*/
/*--[ THEME: BLOG ]--*/
/*----------------------------------------------------------------------------*/

global $paged, $mpc_theme, $mpc_query;

$custom    = mpcth_get_metabox( '_mpcth_custom_settings', false );
$posts_num = $mpc_theme[ 'blog__posts-num' ];
$style     = $mpc_theme[ 'blog__pagination' ];
$filter    = '';

if ( $custom ) {
	$posts_num = mpcth_get_metabox( '_mpcth_posts_number', $posts_num );
	$style     = mpcth_get_metabox( '_mpcth_pagination_type', $style );
	$filter    = mpcth_get_metabox( '_mpcth_filter_posts', $filter );
}

$posts_in = array();
if ( ! empty( $filter ) ) {
	$posts_in[] = array(
		array(
			'taxonomy' 	=> 'category',
			'field' 	=> 'id',
			'terms' 	=> $filter,
		)
	);
}

$mpc_query = new WP_Query();
$mpc_query->query( array(
	'posts_per_page' => $posts_num,
	'post_type'      => 'post',
	'paged'          => $paged,
	'tax_query'      => $posts_in,
) );

get_header();

	echo '<main id="mpcth_main" class="mpcth_main mpcth_blog mpcth_columns--' . $mpc_theme[ 'blog__columns' ] . '" role="main">';

	if ( $mpc_query->have_posts() ) {
		echo '<div id="mpcth_posts" class="mpcth_posts">';
			while ( $mpc_query->have_posts() ) {
				$mpc_query->the_post();

				get_template_part( 'template-parts/blog-post' );
			}
		echo '</div>';

		if ( $mpc_query->max_num_pages > 1 ) {
			echo '<div id="mpcth_pagination" class="mpcth_pagination">';
				include( locate_template( 'template-parts/pagination.php' ) );
			echo '</div>';
		}
	} else {
		get_template_part( 'template-parts/not-found' );
	}

	echo '</main><!-- #mpcth_main -->';

get_sidebar();
get_footer();
