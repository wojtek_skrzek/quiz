<?php
/*----------------------------------------------------------------------------*/
/*--[ THEME: INDEX ]--*/
/*----------------------------------------------------------------------------*/

global $wp_query;

get_header();

	echo '<main id="mpcth_main" class="mpcth_main mpcth_blog" role="main">';

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/archive-post' );
		}

		if ( $wp_query->max_num_pages > 1 ) {
			echo '<div id="mpcth_pagination" class="mpcth_pagination">';
				include( locate_template( 'template-parts/pagination.php' ) );
			echo '</div>';
		}
	} else {
		get_template_part( 'template-parts/not-found' );
	}

	echo '</main><!-- #mpcth_main -->';

get_sidebar();
get_footer();
