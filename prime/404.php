<?php
/*----------------------------------------------------------------------------*/
/*--[ THEME: 404 ]--*/
/*----------------------------------------------------------------------------*/

get_header();

?>

	<main id="mpcth_main" class="mpcth_main" role="main">

		<header class="mpcth_main__header">
			<h1 class="mpcth_title"><?php _e( 'Page not found :(', 'mpcth' ) ?></h1>
		</header>

		<?php get_template_part( 'template-parts/not-found' ); ?>

	</main><!-- #mpcth_main -->

<?php

get_sidebar();
get_footer();
