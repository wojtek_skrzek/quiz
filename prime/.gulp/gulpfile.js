// gulp packages
var gulp    = require( 'gulp' ),
    sass    = require( 'gulp-sass' ),
    concat  = require( 'gulp-concat' ),
	plumber = require( 'gulp-plumber' ),
	uglify  = require( 'gulp-uglify' ),
	rename  = require( 'gulp-rename' ),
	notify  = require( 'gulp-notify' );

// SASS
gulp.task( 'styles', function() {
	return gulp.src( '../assets/scss/**/*.scss' )
		.pipe( plumber( {
			errorHandler: function ( error ) {
				console.log( error );
				this.emit( 'end' );
			}
		} ) )
		.pipe( sass( { outputStyle: 'compact' } ) )
		.pipe( rename( { dirname: '' } ) )
		.pipe( notify( 'SCSS Compiled' ) )
		.pipe( gulp.dest( '..' ) );
} );

// JS
gulp.task( 'uglify', function() {
	return gulp.src( [ '../assets/js/*.js', '!../assets/js/*.min.js' ] )
		.pipe( plumber( {
			errorHandler: function ( error ) {
				console.log( error );
				this.emit( 'end' );
			}
		} ) )
		.pipe( uglify() )
		.pipe( rename( { suffix: '.min' } ) )
		.pipe( gulp.dest( '../assets/js' ) );
} );

// JS LIBS
gulp.task( 'libs-js', function() {
	return gulp.src( '../assets/js/libs/**/*.js' )
		.pipe( concat( 'libs.min.js' ) )
		.pipe( notify( 'JS LIBS Concatenated' ) )
		.pipe( gulp.dest( '../assets/js' ) );
} );
gulp.task( 'libs-css', function() {
	return gulp.src( '../assets/css/libs/**/*.css' )
		.pipe( concat( 'libs.css' ) )
		.pipe( notify( 'CSS LIBS Concatenated' ) )
		.pipe( gulp.dest( '../assets/css' ) );
} );

gulp.task( 'default', [ 'watch' ] );

gulp.task( 'watch', function() {
    gulp.watch( '../assets/scss/**/*.scss', [ 'styles' ] );
    gulp.watch( '../assets/js/libs/**/*.js', [ 'libs-js' ] );
    gulp.watch( '../assets/css/libs/**/*.css', [ 'libs-css' ] );
    gulp.watch( [ '../assets/js/*.js', '!../assets/js/*.min.js' ], [ 'uglify' ] );
} );
