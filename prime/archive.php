<?php
/*----------------------------------------------------------------------------*/
/*--[ THEME: ARCHIVE ]--*/
/*----------------------------------------------------------------------------*/

global $wp_query, $mpc_theme;

$style = $mpc_theme[ 'archive__pagination' ];

get_header();

	echo '<main id="mpcth_main" class="mpcth_main mpcth_archive mpcth_columns--' . $mpc_theme[ 'archive__columns' ] . '" role="main">';

		echo '<header class="mpcth_main__header">';
			echo '<h1 class="mpcth_title">' . get_the_archive_title() . '</h1>';
			echo '<div class="mpcth_description">' . get_the_archive_description() . '</div>';
		echo '</header>';

	if ( have_posts() ) {

		echo '<div id="mpcth_posts" class="mpcth_posts">';
			while ( have_posts() ) {
				the_post();

				get_template_part( 'template-parts/archive-post' );
			}
		echo '</div>';

		if ( $wp_query->max_num_pages > 1 ) {
			echo '<div id="mpcth_pagination" class="mpcth_pagination">';
				include( locate_template( 'template-parts/pagination.php' ) );
			echo '</div>';
		}
	} else {
		get_template_part( 'template-parts/not-found' );
	}

	echo '</main><!-- #mpcth_main -->';

get_sidebar();
get_footer();
