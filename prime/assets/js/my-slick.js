jQuery(document).ready(function(){
    jQuery('#wpk-slick-slider').slick({
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: '<button class="wpk-slick-slider_prev"><i class="fas fa-chevron-left"></i></button>',
        nextArrow: '<button class="wpk-slick-slider_next"><i class="fas fa-chevron-right"></i></button>'

    });
  });