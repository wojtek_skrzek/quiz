/*----------------------------------------------------------------------------*/
/*--[ GLOBALS ]--*/
/*----------------------------------------------------------------------------*/
var _mpc_theme = {
	$window:       jQuery( window ),
	$document:     jQuery( document ),
	$html:         jQuery( 'html' ),
	$body:         jQuery( 'body' ),
	window_width:  window.innerWidth,
	window_height: window.innerHeight,
	is_mobile:     window.innerWidth < 992
};

var mpc = {
	isset: function( obj /*, level1, level2, ... levelN */ ) {
		"use strict";

		if ( typeof obj === 'undefined' ) { return false; }

		for ( var i = 1; i < arguments.length; i++ ) {
			if ( ! obj.hasOwnProperty( arguments[ i ] ) ) { return false; }

			obj = obj[ arguments[ i ] ];
		}

		return true;
	}
};



/*----------------------------------------------------------------------------*/
/*--[ SMART RESIZE ]--*/
/*----------------------------------------------------------------------------*/
(function( $ ) {
	"use strict";

	var _resize_timer;

	if ( _mpc_theme.is_mobile ) {
		_mpc_theme.$html.addClass( 'mpcth_mobile' );
	} else {
		_mpc_theme.$html.removeClass( 'mpcth_mobile' );
	}

	_mpc_theme.$window.on( 'resize', function() {
		clearTimeout( _resize_timer );

		_mpc_theme.window_width  = window.innerWidth;
		_mpc_theme.window_height = window.innerHeight;
		_mpc_theme.is_mobile     = window.innerWidth < 993;

		if ( _mpc_theme.is_mobile ) {
			_mpc_theme.$html.addClass( 'mpcth_mobile' );
		} else {
			_mpc_theme.$html.removeClass( 'mpcth_mobile' );
		}

		_resize_timer = setTimeout( function() {
			_mpc_theme.$window.trigger( 'mpcth.resize' );
		}, 200 );
	} );
})( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ BACK TO TOP ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	var $back_to_top = $( '#mpcth_back_to_top' );

	_mpc_theme.$window.on( 'scroll', function() {
		if ( window.pageYOffset > _mpc_theme.window_height / 2 ) {
			$back_to_top.addClass( 'mpcth_visible' );
		} else {
			$back_to_top.removeClass( 'mpcth_visible' );
		}
	} );

	$back_to_top.on( 'click', function( event ) {
		event.preventDefault();

		_mpc_theme.$body.animate( { 'scrollTop': 0 } );
	} );
} )( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ PAGE HEIGHT ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	_mpc_theme.$window.on( 'load mpcth.resize', function() {
		var frame   = document.getElementById( 'mpcth_frame' ),
			bar     = document.getElementById( 'wpadminbar' ),
			_frame  = frame !== null ? frame.scrollHeight * 2 : 0,
			_bar    = bar !== null ? bar.scrollHeight : 0,
			_height = _frame + document.getElementById( 'mpcth_page_footer' ).scrollHeight + _bar;

		document.getElementById( 'mpcth_page_content' ).style.minHeight = _mpc_theme.window_height - _height + 'px';
	} );
} )( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ FIXED HEADER ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	function update_sticky_point() {
		_sticky_point = $sticky_point.offset().top + _offset;

		if ( _is_sticky && _is_offset ) {
			_sticky_point += $logo.height();
		}

		if ( _is_float && _is_pre_header ) {
			if ( $pre_header.find( '.vc_row-o-full-height' ).length ) {
				$header.css( 'margin-top', - _space_height );

				_sticky_point = _sticky_point - _space_height;
			}
		}
	}

	function sticky_header() {
		if ( _mpc_theme.is_mobile ) {
			$header.removeClass( 'mpcth_floating' );

			if ( ! _is_float ) {
				$after_header.css( 'padding-top', 0 );
			}

			return;
		}

		if ( _mpc_theme.$document.scrollTop() > _sticky_point ) {
			$header.addClass( 'mpcth_floating' );

			if ( ! _is_float ) {
				$after_header.css( 'padding-top', _space_height );
			}
		} else {
			$header.removeClass( 'mpcth_floating' );

			if ( ! _is_float ) {
				$after_header.css( 'padding-top', 0 );
			}
		}
	}

	var $header = $( '#mpcth_page_header' );

	if ( ! $header.length ) { return; }

	var $after_header  = $header.next(),
		$pre_header    = $( '#mpcth_pre_header' ),
		$sticky_point  = $( '#mpcth_page_header__anchor' ),
		$logo          = $( '.mpcth_logo-wrap' ),
		_space_height  = $header.outerHeight(),
		_offset        = - parseInt( _mpc_theme.$html.css( 'padding-top' ) ) + parseInt( $header.css( 'margin-top' ) ),
		_sticky_point  = $sticky_point.offset().top + _offset,
		_is_sticky     = ! $header.is( '.mpcth_no-sticky' ),
		_is_pre_header = $pre_header.length !== 0,
		_is_float      = $header.is( '.mpcth_float' ),
		_is_offset     = $header.find( '.mpcth_header-layout.mpcth_layout--layout_02' ).length !== 0;

	if ( _is_sticky && _is_offset ) {
		_sticky_point += $logo.height();
	}

	if ( _is_pre_header ) {
		$pre_header.find( '.wpb_row' ).last().css( 'margin-bottom', 0 );
	}

	if ( _is_sticky ) {
		_mpc_theme.$document.on( 'scroll', sticky_header ).trigger( 'scroll' );
	}

	_mpc_theme.$window.on( 'load mpcth.resize', update_sticky_point );

	update_sticky_point();
} )( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ FIXED SIDEBAR ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	/// maybe...
} )( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ FIXED FOOTER ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	if ( ! _mpc_theme.$body.is( '.mpcth_footer--fixed' ) ) {
		return;
	}

	_mpc_theme.$window.on( 'load', function() {
		$( '#mpcth_page_content' ).css( 'margin-bottom', $( '#mpcth_page_footer' )[ 0 ].scrollHeight );
	} );
} )( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ FORMATS: GALLERY ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	function init_gallery( index, elem ) {
		var $gallery = $( elem ),
			_nav_id  = $gallery[ 0 ].getAttribute( 'data-nav-id' );

		if ( $gallery.is( '.slick-initialized' ) ) {
			return;
		}

		$gallery.slick( {
			adaptiveHeight: true,
			prevArrow: '[data-nav-id=' + _nav_id + '] .mpcth_prev',
			nextArrow: '[data-nav-id=' + _nav_id + '] .mpcth_next'
		} );
	}

	var $galleries = $( '.mpcth_gallery' ),
		$grids     = $( '#mpcth_posts' );

	$galleries.each( init_gallery );

	$grids.on( 'arrangeComplete mpc.load_more', function() {
		var $grid = $( this );

		$grid.find( '.mpcth_gallery' ).each( init_gallery );
	} );

} )( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ BLOG & PORTFOLIO: GRID ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	var $grid = $( '#mpcth_posts' );

	if ( $grid.find( '.mpcth_grid-item' ).length ) {
		$grid.imagesLoaded().always( function() {
			$grid.isotope( {
				itemSelector: '.mpcth_grid-item'
			} );
		} ).on( 'mpc.load_more', function() {
			$grid.isotope( 'layout' );
		} );
	}
} )( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ SHOP: GRID ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	if ( $( '#mpcth_main.mpcth_shop:not(.mpcth_columns--1)' ).length === 0 ) {
		return;
	}

	var $grid = $( '.products' );

	if ( $grid.find( '.product' ).length ) {
		$grid.imagesLoaded().always( function() {
			$grid.isotope( {
				itemSelector: '.product'
			} );
		} );
	}
} )( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ PAGINATION: LOAD MORE ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	var $load_more   = $( '#mpcth_load_more' );

	if ( $load_more.length ) {
		var $wrap        = $( '<div>' ),
			$main        = $( '#mpcth_posts' ),
			_is_infinite = $load_more.is( '.mpcth_infinite' ),
			_is_loading  = false,
			_is_finished = false,
			_is_grid     = !!$main.find( '.mpcth_grid-item' ).length,
			_target      = _is_grid ? '.mpcth_grid-item' : '.mpcth_post',
			_link        = $load_more[ 0 ].getAttribute( 'href' ),
			_next        = parseInt( $load_more[ 0 ].getAttribute( 'data-current' ) ) + 1,
			_total       = parseInt( $load_more[ 0 ].getAttribute( 'data-total' ) ),
			$items;

		if ( _next > _total ) {
			_is_finished = true;

			$load_more.remove();
		} else {
			$load_more.on( 'click', function( event ) {
				event.preventDefault();

				if ( ! _is_loading ) {
					_is_loading = true;

					$wrap.load( _link.replace( '%#%', _next ) + ' #mpcth_posts ' + _target, function() {
						_is_loading = false;

						$items = $wrap.children();

						$main.append( $items );

						if ( _is_grid ) {
							$main.isotope( 'appended', $items );

							$main.imagesLoaded().always( function() {
								$main.trigger( 'mpc.load_more' );
							} );
						}

						if ( _next + 1 < _total ) {
							_next++;
						} else {
							_is_finished = true;

							$load_more.remove();
						}
					} );
				}
			} );

			if ( _is_infinite ) {
				$main.on( 'mpc.load_more', function() {
					_mpc_theme.$window.trigger( 'mpc.delay_scroll' );
				} );

				_mpc_theme.$window.on( 'scroll load mpc.delay_scroll', function() {
					if ( ! _is_loading ) {
						var _top = $load_more.offset().top,
							_viewport = document.body.scrollTop + _mpc_theme.window_height;

						if ( _top < _viewport ) {
							$load_more.trigger( 'click' );
						}
					}
				} );
			}
		}
	}
} )( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ MEGA MENU ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	function update_position() {
		var _window_width = document.documentElement.clientWidth;

		$mega_menu_wraps.each( function() {
			var $mega_menu_wrap = $( this ),
				_row_width      = $mega_menu_wrap.data( '_row_width' ),
				_sides_width    = $mega_menu_wrap.data( '_sides_width' ),
				_menu_width     = _row_width + _sides_width,
				_offset         = $mega_menu_wrap.parent().offset().left;

			if ( _menu_width > _window_width || _menu_width + _base_offset > _window_width ) {
				$mega_menu_wrap
					.css( 'width', _window_width )
					.addClass( 'mpc-shrink' );

				$mega_menu_wrap.css( 'left', - _offset );
			} else {
				$mega_menu_wrap
					.css( 'width', _menu_width )
					.removeClass( 'mpc-shrink' );

				if ( _offset + _menu_width > _window_width ) {
					$mega_menu_wrap.css( 'left', _window_width - ( _offset + _base_offset + _menu_width ) );
				} else {
					$mega_menu_wrap.css( 'left', 0 );
				}
			}
		} );
	}

	function update_full_width() {
		var _window_width = document.documentElement.clientWidth;

		$full_width_wraps.each( function() {
			var $mega_menu_wrap = $( this ),
				_offset         = $mega_menu_wrap.parent().offset().left;

			$mega_menu_wrap
				.css( 'width', _window_width )
				.css( 'left', - _offset );
		} );
	}

	function update_scroll() {
		$mega_menu_wraps.each( set_max_height );
		$full_width_wraps.each( set_max_height );
	}

	function set_max_height( index, elem ) {
		var $mega_menu_wrap = $( elem ),
			_offset         = $mega_menu_wrap.offset().top,
			_window_height  = document.documentElement.clientHeight;

		$mega_menu_wrap.children( '.sub-menu' ).css( 'max-height', _window_height - _offset );
	}

	var $main_menu        = $( '#mpcth_page_navigation' ),
		$mega_menu_wraps  = $main_menu.find( '.mpc-mega-menu:not(.mpc-full-width) .mpc-mega-menu-wrap' ),
		$full_width_wraps = $main_menu.find( '.mpc-mega-menu.mpc-full-width .mpc-mega-menu-wrap' ),
		_base_offset      = 30;

	$main_menu.find( '.mpc-menu a[data-width]' ).each( function() {
		var $link = $( this );

		if ( typeof $link.attr( 'data-width' ) !== 'undefined' ) {
			$link.next( '.sub-menu' ).width( $link.attr( 'data-width' ) );
		}
	} );

	$mega_menu_wraps.each( function() {
		var $mega_menu_wrap = $( this ),
			$columns        = $mega_menu_wrap.find( '.mpc-mega-menu-col' ),
			$break          = $mega_menu_wrap.find( '.mpc-mega-menu-break' ),
			_row_width      = 0;

		if ( $break.length ) {
			$columns = $columns.slice( 0, $break.index() );
		}

		$columns.each( function() {
			_row_width += parseInt( this.getAttribute( 'data-width' ) ) || 200;
		} );

		$mega_menu_wrap.width( _row_width );

		$mega_menu_wrap.data( '_row_width', _row_width );
		$mega_menu_wrap.data( '_sides_width', $mega_menu_wrap.outerWidth() - _row_width );
	} );

	_mpc_theme.$window.on( 'load mpcth.resize', function() {
		if ( _mpc_theme.is_mobile ) {
			$mega_menu_wraps.css( {
				'width':      '',
				'left':       '',
				'max-height': ''
			} );
			$full_width_wraps.css( {
				'width':      '',
				'left':       '',
				'max-height': ''
			} );
		} else {
			update_position();
			update_full_width();
			update_scroll();
		}
	} );
} )( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ CLASSIC MENU ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	function check_flip_sides() {
		var _window_width = document.documentElement.clientWidth;

		$last_sub_menus.each( function() {
			var _right = this.getBoundingClientRect().right;

			if ( _right > _window_width ) {
				this.classList.add( 'mpc-flip-left' );
			} else {
				this.classList.remove( 'mpc-flip-left' );
			}
		} );
	}

	function update_flip_sides() {
		$top_menu_items.each( function() {
			if ( this.querySelector( '.mpc-flip-left' ) !== null ) {
				this.classList.add( 'mpc-flip-menus' );
			} else {
				this.classList.remove( 'mpc-flip-menus' );
			}
		} );
	}

	var $main_menu      = $( '#mpcth_page_navigation' ),
		$last_sub_menus = $main_menu.find( '.mpc-menu > .menu-item:not(.mpc-mega-menu) .sub-menu' ).filter( function() { return $( '.sub-menu', this ).length === 0; } ),
		$top_menu_items = $main_menu.find( '.mpc-menu > .menu-item:not(.mpc-mega-menu)' );

	_mpc_theme.$window.on( 'load mpcth.resize', function() {
		$top_menu_items.removeClass( 'mpc-flip-menus' );

		if ( _mpc_theme.is_mobile ) {
			$last_sub_menus.removeClass( 'mpc-flip-left' );
		} else {
			check_flip_sides();
			update_flip_sides();
		}
	} );
} )( jQuery );



/*----------------------------------------------------------------------------*/
/*--[ MOBILE MENU ]--*/
/*----------------------------------------------------------------------------*/
( function( $ ) {
	"use strict";

	var $header        = $( '#mpcth_page_header' ),
		$menu          = $( '#mpcth_page_navigation' ),
		$menu_toggle   = $( '#mpcth_navigation_toggle' ),
		$links         = $menu.find( '> ul > li > a' ),
		$sub_menus     = $links.siblings( '.sub-menu, .mpc-mega-menu-wrap' ),
		$search        = $( '#mpcth_page_search' ),
		$search_toggle = $( '#mpcth_search_toggle' );

	$menu_toggle.on( 'click', function( event ) {
		event.preventDefault();

		$header.attr( 'data-mode', $header.attr( 'data-mode' ) !== 'menu' ? 'menu' : '' );

		if ( $header.attr( 'data-mode' ) !== 'menu' ) {
			$links.removeClass( 'mpcth_active' );
			$sub_menus.css( 'height', '' );
			_mpc_theme.$body.css( 'overflow', '' );
		} else {
			_mpc_theme.$body.css( 'overflow', 'hidden' );
		}
	} );

	$search_toggle.on( 'click', function( event ) {
		event.preventDefault();

		$header.attr( 'data-mode', $header.attr( 'data-mode' ) !== 'search' ? 'search' : '' );

		_mpc_theme.$body.css( 'overflow', '' );
	} );

	$links.on( 'click', function( event ) {
		var $link     = $( this ),
			$sub_menu = $link.siblings( '.sub-menu, .mpc-mega-menu-wrap' );

		if ( ! $sub_menu.length ) {
			return;
		}

		if ( $link.is( '.mpcth_active[href="#"]' ) ) {
			event.preventDefault();

			$link.removeClass( 'mpcth_active' );

			$sub_menu.css( 'height', '' );
		} else if ( ! $link.is( '.mpcth_active' ) ) {
			event.preventDefault();

			$links.removeClass( 'mpcth_active' );
			$link.addClass( 'mpcth_active' );

			$sub_menus.css( 'height', '' );
			$sub_menu.css( 'height', $sub_menu.prop( 'scrollHeight' ) + 50 ); // magic offset
		} else {
			$link.removeClass( 'mpcth_active' );
		}
	} );

	_mpc_theme.$window.on( 'load mpcth.resize', function() {
		if ( _mpc_theme.is_mobile ) {
			$menu.css( 'max-height', _mpc_theme.window_height - $header.height() );
		} else {
			$links.removeClass( 'mpcth_active' );

			$sub_menus.css( 'height', '' );

			$header.attr( 'data-mode', '' );
		}
	} );
} )( jQuery );