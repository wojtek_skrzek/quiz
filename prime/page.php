<?php
/*----------------------------------------------------------------------------*/
/*--[ THEME: PAGE ]--*/
/*----------------------------------------------------------------------------*/

$disable_title = mpcth_get_metabox( '_mpcth_disable_title', false );

get_header();

	echo '<main id="mpcth_main" class="mpcth_main" role="main">';

	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();

			?>

			<article id="page_<?php the_ID(); ?>" <?php post_class( 'mpcth_post' ); ?>>
				<?php if ( ! $disable_title ) : ?>
					<header class="mpcth_post__header">
						<?php
						mpcth_display_breadcrumb();

						the_title( '<h1 class="mpcth_post__title">', '</h1>' );
						?>
					</header>
				<?php endif; ?>

				<div class="mpcth_post__content">
					<?php the_content(); ?>
				</div>

				<footer class="mpcth_post__footer">
					<?php edit_post_link( esc_html__( 'Edit', 'mpcth' ), '<div class="mpcth_signel__edit">', '</div>' ); ?>
				</footer>
			</article>

			<?php
		}
	} else {
		get_template_part( 'template-parts/not-found' );
	}

	echo '</main>';

get_sidebar();
get_footer();
