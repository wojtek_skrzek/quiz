<?php

// WORDPRESS VERSION CHECK
if ( version_compare( $GLOBALS[ 'wp_version' ], '4.4', '<' ) ) {
	require get_template_directory() . '/inc/disable.php';
}

// THEME LOCATIONS
define( 'MPC_THEME_DIR', get_template_directory() );
define( 'MPC_THEME_URI', get_template_directory_uri() );
define( 'MPC_THEME_STYLE_DIR', get_stylesheet_directory() );
define( 'MPC_THEME_STYLE_URI', get_stylesheet_directory_uri() );

// PANEL SETTINGS
global $mpc_theme;

// LOCALIZATION
if ( ! is_textdomain_loaded( 'mpcth' ) ) {
	load_theme_textdomain( 'mpcth', get_template_directory() . '/languages' );
}

// SETUP THEME
add_action( 'after_setup_theme', 'mpcth_setup' );
function mpcth_setup() {
	global $mpc_theme;

	if ( ! is_array( $mpc_theme ) || count( $mpc_theme ) < 10 ) {
		if ( file_exists( MPC_THEME_DIR . '/panel/config.json' ) ) {
			$panel_json = file_get_contents( MPC_THEME_DIR . '/panel/config.json' );

			if ( $panel_json ) {
				$mpc_theme = json_decode( $panel_json, true );
			}
		}
	}

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'massive-addons' );

	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );

	if ( $mpc_theme[ 'header__layout' ] != 'layout_05' ) {
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'mpcth' ),
		) );
	} else {
		register_nav_menus( array(
			'primary-left' => esc_html__( 'Primary Menu - Left', 'mpcth' ),
			'primary-right' => esc_html__( 'Primary Menu - Right', 'mpcth' ),
		) );
	}

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	add_theme_support( 'post-formats', array(
		'aside',
		'gallery',
		'link',
		'image',
		'quote',
		'status',
		'video',
		'audio',
		'chat',
	) );

	if ( defined( 'WPB_VC_VERSION' ) ) {
		vc_set_default_editor_post_types( array(
			'post',
			'mpc_portfolio',
			'page'
		) );
	}

	add_image_size( 'mpcth-thumb--large', 1200, 9999 );
}

// DEFINE CONTENT WIDTH
add_action( 'after_setup_theme', 'mpcth_content_width', 0 );
function mpcth_content_width() {
	$GLOBALS[ 'content_width' ] = apply_filters( 'mpcth_content_width', 640 );
}

// REGISTER WIDGET AREAS
add_action( 'widgets_init', 'mpcth_widgets_init' );
function mpcth_widgets_init() {
	global $mpc_theme;

	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'mpcth' ),
		'id'            => 'sidebar-main',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget mpcth_widget mpcth_widget__text %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widget-title mpcth_widget__title">',
		'after_title'   => '</h5>',
	) );

	if ( $mpc_theme[ 'widgets__columns' ] == 'fluid' ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Footer', 'mpcth' ),
			'id'            => 'footer-main',
			'description'   => '',
			'before_widget' => '<div id="%1$s" class="widget mpcth_widget mpcth_column mpcth_widget__text %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h5 class="widget-title mpcth_widget__title">',
			'after_title'   => '</h5>',
		) );
	} else {
		$columns = intval( $mpc_theme[ 'widgets__columns' ] ) ?: 4;

		for ( $i = 1; $i <= $columns; $i++ ) {
			register_sidebar( array(
				'name'          => esc_html__( 'Footer - Column', 'mpcth' ) . ' ' . $i,
				'id'            => 'footer-main-' . $i,
				'description'   => '',
				'before_widget' => '<div id="%1$s" class="widget mpcth_widget mpcth_widget__text %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="widget-title mpcth_widget__title">',
				'after_title'   => '</h5>',
			) );
		}
	}
}

// ADD SHORTCODES SUPPORT TO TEXT WIDGET
add_filter( 'widget_text', 'do_shortcode' );

// ENQUEUE STYLES AND SCRIPTS
add_action( 'wp_enqueue_scripts', 'mpcth_enqueue' );
function mpcth_enqueue() {
	wp_enqueue_style( 'mpcth-style', get_stylesheet_uri() );
	wp_enqueue_style( 'mpcth-libs', MPC_THEME_URI . '/assets/css/libs.css' );
	wp_enqueue_style( 'fontawesome-css', MPC_THEME_URI . '/assets/fontawesomepro/css/fontawesome-all.min.css' );
	wp_enqueue_style( 'slick-css', MPC_THEME_URI . '/assets/slick/slick.css' );
	//wp_enqueue_style( 'slick-theme-css', MPC_THEME_URI . '/assets/slick/slick-theme.css' );

	wp_enqueue_style( 'mpcth-icons-socials', MPC_THEME_URI . '/assets/fonts/socials.css' );
	wp_enqueue_style( 'mpcth-icons-theme', MPC_THEME_URI . '/assets/fonts/theme.css' );

	$blog_id = is_multisite() ? '_' . get_current_blog_id() : '';
	if ( file_exists( MPC_THEME_STYLE_DIR . '/style_custom' . $blog_id . '.css' ) ) {
		wp_enqueue_style( 'theme-style-custom', MPC_THEME_STYLE_URI . '/style_custom' . $blog_id . '.css' );
	}

	wp_enqueue_script( 'mpcth-libs', MPC_THEME_URI . '/assets/js/libs.min.js', array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'mpcth-script', MPC_THEME_URI . '/assets/js/main.js', array( 'jquery', 'mpcth-libs' ), '1.0', true );
	wp_enqueue_script( 'slick-script', MPC_THEME_URI . '/assets/slick/slick.min.js', array(), '', true );
	wp_enqueue_script( 'my-slick-script', MPC_THEME_URI . '/assets/js/my-slick.js', array(), '', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	mpcth_custom_styles();
}

// PRINT CUSTOM STYLES
function mpcth_custom_styles() {
	global $mpc_theme;

	$custom_styles = '';

	if ( mpcth_get_metabox( '_mpcth_transparent_header', false ) ) {
		$opacity = mpcth_get_metabox( '_mpcth_header_opacity', 100 );

		if ( $opacity != 100 ) {
			$custom_styles .= '.mpcth_page__header .mpcth_background-target{opacity:' . ( $opacity / 100 ) . ';}';
		}
	}

	wp_add_inline_style( 'mpcth-style', apply_filters( 'mpcth/css/header', $custom_styles ) );
}

// DISABLE SIZE VARIATION IN TAG CLOUD WIDGET
add_filter( 'widget_tag_cloud_args', 'mpcth_tag_cloud_sizes' );
function mpcth_tag_cloud_sizes( $args ) {
	$args[ 'largest' ]  = 1;
	$args[ 'smallest' ] = 1;
	$args[ 'unit' ]     = 'em';

	return $args;
}

// ADDITIONAL FUNCTIONS
require( 'inc/extras.php' );

// MARKUP PARTS
require( 'inc/parts.php' );

// PLUGINS ACTIVATION
require( 'inc/plugins.php' );

// WOOCOMMERCE
if ( class_exists( 'WooCommerce' ) ) {
	require( 'inc/woocommerce.php' );
}