<?php
	if ( ! class_exists( 'Redux' ) ) {
		return;
	}

	Redux::setExtensions( MPC_THEME_OPTIONS, get_template_directory() . '/panel/extensions/' );

	$args = array(
		'opt_name'             => MPC_THEME_OPTIONS,
		'display_name'         => __( 'Theme Panel', 'mpcth' ),
		'display_version'      => '0.1',
		'menu_type'            => 'menu',
		'allow_sub_menu'       => true,
		'menu_title'           => __( 'Theme Panel', 'mpcth' ),
		'page_title'           => __( 'Theme Panel', 'mpcth' ),
		'google_api_key'       => 'AIzaSyAFJ3AraiITu49dSxFu9YA4WPOsQr6FFb8',
		'google_update_weekly' => true,
		'async_typography'     => false,
		'admin_bar'            => true,
		'admin_bar_icon'       => 'dashicons-portfolio',
		'admin_bar_priority'   => 50,
		'global_variable'      => '',
		'dev_mode'             => false,
		'update_notice'        => false,
		'customizer'           => true,
		'disable_save_warn' => true,
		'page_priority'        => null,
		'page_parent'          => 'themes.php',
		'page_permissions'     => 'manage_options',
		'menu_icon'            => '',
		'last_tab'             => '',
		'page_icon'            => 'icon-themes',
		'page_slug'            => '',
		'save_defaults'        => true,
		'default_show'         => false,
		'default_mark'         => '',
		'show_import_export'   => true,
		'transient_time'       => 60 * MINUTE_IN_SECONDS,
		'output'               => true,
		'output_tag'           => true,
		'database'             => '',
		'allow_tracking'       => false,
		'forced_dev_mode_off'  => true,
	);

	$args['share_icons'][] = array(
		'url'   => 'https://www.facebook.com/massivepixelcreation',
		'title' => __( 'Like us on Facebook', 'mpcth' ),
		'icon'  => 'el-icon-facebook'
	);
	$args['share_icons'][] = array(
		'url'   => 'https://twitter.com/mpcreation',
		'title' => __( 'Follow us on Twitter', 'mpcth' ),
		'icon'  => 'el-icon-twitter'
	);
	$args['share_icons'][] = array(
		'url'   => 'https://mpc.ticksy.com',
		'title' => __( 'Visit our Support', 'mpcth' ),
		'icon'  => 'el-icon-group'
	);

	Redux::setArgs( MPC_THEME_OPTIONS, $args );

/* Global */
require get_template_directory() . '/panel/config/global.php';
/* Global - Socials */
require get_template_directory() . '/panel/config/socials.php';
/* Global - Breadcrumbs */
require get_template_directory() . '/panel/config/breadcrumbs.php';
/* Global - Pagination */
require get_template_directory() . '/panel/config/pagination.php';
/* Global - Inputs */
require get_template_directory() . '/panel/config/inputs.php';

/* Header */
require get_template_directory() . '/panel/config/header.php';
/* Header - Logo */
require get_template_directory() . '/panel/config/header__logo.php';
/* Header - Menu */
require get_template_directory() . '/panel/config/header__menu.php';

/* Sidebar */
require get_template_directory() . '/panel/config/sidebar.php';

/* Footer */
require get_template_directory() . '/panel/config/footer.php';
/* Footer - Widgets */
require get_template_directory() . '/panel/config/footer__widgets.php';
/* Footer - Copyrights */
require get_template_directory() . '/panel/config/footer__copyrights.php';
/* Footer - To Top */
require get_template_directory() . '/panel/config/footer__to_top.php';

/* Blog */
require get_template_directory() . '/panel/config/blog.php';

/* Portfolio */
require get_template_directory() . '/panel/config/portfolio.php';

/* Archive */
require get_template_directory() . '/panel/config/archive.php';

if ( class_exists( 'WooCommerce' ) ) {
	/* WooCommerce */
	require get_template_directory() . '/panel/config/woocommerce.php';
	/* WooCommerce - Shop */
	require get_template_directory() . '/panel/config/woocommerce-shop.php';
	/* WooCommerce - Product */
	require get_template_directory() . '/panel/config/woocommerce-product.php';
	/* WooCommerce - Mini Cart */
	require get_template_directory() . '/panel/config/woocommerce-cart.php';
}

/* Mobile */
require get_template_directory() . '/panel/config/mobile.php';
/* Mobile - Header */
require get_template_directory() . '/panel/config/mobile__header.php';
/* Mobile - Logo */
require get_template_directory() . '/panel/config/mobile__logo.php';
/* Mobile - Menu */
require get_template_directory() . '/panel/config/mobile__menu.php';

/* Custom */
require get_template_directory() . '/panel/config/custom.php';



// SAVE CUSTOM STYLES TO FILE
add_filter( 'redux/options/' . MPC_THEME_OPTIONS . '/compiler', 'mpcth_custom_styles_save', 10, 3 );
function mpcth_custom_styles_save( $options, $css, $changed_values ) {
	global $mpc_theme;

	$divider_styles = array(
		'style_01' => '',
		'style_02' => '\002F',
		'style_03' => '\005C',
		'style_04' => '\007C',
		'style_05' => '\003E',
		'style_06' => '\003A',
		'style_07' => '\003A\003A',
	);

	// PAGE DISPLAY
	if ( $mpc_theme[ 'global__display' ] == 'frame' && $mpc_theme[ 'global__frame' ] != '0' ) {
		$css .= 'html{padding:' . $mpc_theme[ 'global__frame' ] . 'px;}';
		$css .= '.mpcth_page__header.mpcth_floating{top:' . $mpc_theme[ 'global__frame' ] . 'px;}';
		$css .= '.mpcth_frame{height:' . $mpc_theme[ 'global__frame' ] . 'px;}';
		$css .= '.mpcth_footer--fixed .mpcth_page__footer{left:' . $mpc_theme[ 'global__frame' ] . 'px;right:' . $mpc_theme[ 'global__frame' ] . 'px;margin-bottom:' . $mpc_theme[ 'global__frame' ] . 'px;}';
		$css .= '.mpcth_display--frame .mpcth_page__header.mpcth_floating{left:' . $mpc_theme[ 'global__frame' ] . 'px;right:' . $mpc_theme[ 'global__frame' ] . 'px;}';
	}

	// SOCIALS
	if ( ! empty( $mpc_theme[ 'socials__size' ] ) ) {
		$css .= '.mpcth_page a.social{font-size:' . $mpc_theme[ 'socials__size' ] . 'px;}';
	}

	// BREADCRUMBS
	if ( isset( $divider_styles[ $mpc_theme[ 'breadcrumbs__divider-style' ] ] ) ) {
		$css .= '.mpcth_breadcrumbs .mpcth_separator:before{content:"' . $divider_styles[ $mpc_theme[ 'breadcrumbs__divider-style' ] ] . '"}';
	}

	// PAGINATION
	if ( isset( $divider_styles[ $mpc_theme[ 'pagination__divider-style' ] ] ) ) {
		$css .= '.mpcth_pagination .mpcth_separator:before{content:"' . $divider_styles[ $mpc_theme[ 'pagination__divider-style' ] ] . '"}';
	}

	// HEADER
	if ( $mpc_theme[ 'header__float' ] && $mpc_theme[ 'header__offset' ] != '0' ) {
		$css .= '.mpcth_page__header{margin-top:' . $mpc_theme[ 'header__offset' ] . 'px;}';
	}

	$css .= '.mpcth_page__header{border-bottom-width:' . $mpc_theme[ 'header__divider-width' ] . 'px;}';

	// LOGO
	if ( ! empty( $mpc_theme[ 'logo__image-height' ][ 'height' ] ) ) {
		$css .= '.mpcth_logo img{height:' . $mpc_theme[ 'logo__image-height' ][ 'height' ] . 'px;}';
	}
	if ( ! empty( $mpc_theme[ 'mobile_logo__image-height' ][ 'height' ] ) ) {
		$css .= '.mpcth_logo.mpcth_mobile img{height:' . $mpc_theme[ 'mobile_logo__image-height' ][ 'height' ] . 'px;}';
	}

	// MENU
	if ( isset( $divider_styles[ $mpc_theme[ 'menu__divider-style' ] ] ) ) {
		$css .= '.mpc-menu > .menu-item:before{content:"' . $divider_styles[ $mpc_theme[ 'menu__divider-style' ] ] . '";}';
	}

	$css.= '.mpcth_page__navigation .current-menu-item > a{color:' . $mpc_theme[ 'menu__font-color' ][ 'hover' ] . ';}';
	$css.= '.mpcth_page__navigation .current-menu-ancestor:not(.mpc-mega-menu-col) > a{color:' . $mpc_theme[ 'menu__font-color' ][ 'hover' ] . ';}';
	$css.= '.mpcth_page__navigation .mpc-mega-menu-col .sub-menu .current-menu-item > a{color:' . $mpc_theme[ 'menu__mega-menu-color' ][ 'hover' ] . ';}';
	$css.= '.mpcth_page__navigation .mpc-mega-menu-col .sub-menu .current-menu-ancestor > a{color:' . $mpc_theme[ 'menu__mega-menu-color' ][ 'hover' ] . ';}';

	$css.= '.mpcth_mobile .mpcth_page__navigation .current-menu-item > a{color:' . $mpc_theme[ 'mobile_menu__font-color' ][ 'hover' ] . ';}';
	$css.= '.mpcth_mobile .mpcth_page__navigation .current-menu-ancestor > a{color:' . $mpc_theme[ 'mobile_menu__font-color' ][ 'hover' ] . ';}';
	$css.= '.mpcth_mobile .mpcth_page__navigation .mpc-mega-menu-col .sub-menu .current-menu-item > a{color:' . $mpc_theme[ 'mobile_menu__font-color' ][ 'hover' ] . ';}';
	$css.= '.mpcth_mobile .mpcth_page__navigation .mpc-mega-menu-col .sub-menu .current-menu-ancestor > a{color:' . $mpc_theme[ 'mobile_menu__font-color' ][ 'hover' ] . ';}';

	// BLOG GRID
	if ( $mpc_theme[ 'blog__gap' ] != '0' ) {
		if ( $mpc_theme[ 'blog__columns' ] != '1' ) {
			$css .= '.mpcth_blog .mpcth_grid-item{padding-right:' . $mpc_theme[ 'blog__gap' ] . 'px;padding-bottom:' . $mpc_theme[ 'blog__gap' ] . 'px;}';
			$css .= '.mpcth_blog .mpcth_posts{margin-right:-' . $mpc_theme[ 'blog__gap' ] . 'px;}';
		} else {
			$css .= '.mpcth_blog .mpcth_post{margin-bottom:' . $mpc_theme[ 'blog__gap' ] . 'px;}';
		}
	}

	// PORTFOLIO GRID
	if ( $mpc_theme[ 'portfolio__gap' ] != '0' ) {
		if ( $mpc_theme[ 'portfolio__columns' ] != '1' ) {
			$css .= '.mpcth_portfolio .mpcth_grid-item{padding-right:' . $mpc_theme[ 'portfolio__gap' ] . 'px;padding-bottom:' . $mpc_theme[ 'portfolio__gap' ] . 'px;}';
			$css .= '.mpcth_portfolio .mpcth_posts{margin-right:-' . $mpc_theme[ 'portfolio__gap' ] . 'px;}';
		} else {
			$css .= '.mpcth_portfolio .mpcth_post{margin-bottom:' . $mpc_theme[ 'portfolio__gap' ] . 'px;}';
		}
	}

	// ARCHIVE GRID
	if ( $mpc_theme[ 'archive__gap' ] != '0' ) {
		if ( $mpc_theme[ 'archive__columns' ] != '1' ) {
			$css .= '.mpcth_archive .mpcth_grid-item{padding-right:' . $mpc_theme[ 'archive__gap' ] . 'px;padding-bottom:' . $mpc_theme[ 'archive__gap' ] . 'px;}';
			$css .= '.mpcth_archive .mpcth_posts{margin-right:-' . $mpc_theme[ 'archive__gap' ] . 'px;}';
		} else {
			$css .= '.mpcth_archive .mpcth_post{margin-bottom:' . $mpc_theme[ 'archive__gap' ] . 'px;}';
		}
	}

	// FOOTER
	$css .= '.mpcth_page__footer{border-top-width:' . $mpc_theme[ 'footer__divider-width' ] . 'px;}';

	// BACK TO TOP
	$css .= '.mpcth_back-to-top{border-radius:' . $mpc_theme[ 'to_top__radius' ] . 'px;}';

	// FILTERS
	$css = apply_filters( 'mpcth/css/file', $css );

	// CUSTOM CSS
	if ( trim( $mpc_theme[ 'css_editor' ] ) != '' ) {
		$css .= trim( $mpc_theme[ 'css_editor' ] );
	}

	$blog_id = is_multisite() ? '_' . get_current_blog_id() : '';
	$saved_file = file_put_contents( MPC_THEME_STYLE_DIR . '/style_custom' . $blog_id . '.css', $css );

	if ( $saved_file ) {
		// Do something smart...
	} else {
		// Inform about an issue...
	}
}