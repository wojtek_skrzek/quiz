<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: PAGINATION ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Pagination', 'mpcth' ),
	'id'         => 'pagination__section',
	'desc'       => __( 'Settings for pagination.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'pagination__load-more',
			'type'     => 'text',
			'title'    => __( 'Load More Text', 'mpcth' ),
			'subtitle' => __( 'Specify load more text.', 'mpcth' ),
			'default'  => __( 'Load More', 'mpcth' ),
		),

		//----------------------------------------------------------------------------//
		//	DIVIDER
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'pagination__divider-start',
			'type'     => 'section',
			'title'    => __( 'Divider', 'mpcth' ),
			'subtitle' => __( 'Specify divider setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'pagination__divider-style',
			'type'     => 'button_set',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify divider style.', 'mpcth' ),
			'options'  => array(
				'style_02' => '&#47;',
				'style_03' => '&#92;',
				'style_04' => '&#124;',
				'style_05' => '&#62;',
				'style_06' => '&#58;',
				'style_07' => '&#58;&#58;',
			),
			'default'  => 'style_02',
		),
		array(
			'id'       => 'pagination__divider-color',
			'type'     => 'color',
			'title'    => __( 'Color', 'mpcth' ),
			'subtitle' => __( 'Specify divider color.', 'mpcth' ),
			'default'  => '',
			'validate' => false,
			'compiler' => array( '.mpcth_pagination .mpcth_separator' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'     => 'pagination__divider-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	TYPOGRAPHY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'pagination__font-start',
			'type'     => 'section',
			'title'    => __( 'Typography', 'mpcth' ),
			'subtitle' => __( 'Specify font setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'             => 'pagination__font-content',
			'type'           => 'typography',
			'title'          => __( 'Content', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify content font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_pagination' ),
		),
		array(
			'id'          => 'pagination__font-color',
			'type'        => 'link_color',
			'title'       => __( 'Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_pagination a' ),
		),
		array(
			'id'     => 'pagination__font-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
