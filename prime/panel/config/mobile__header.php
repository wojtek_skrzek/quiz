<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: MOBILE - HEADER ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Header', 'mpcth' ),
	'id'         => 'mobile_header__section',
	'desc'       => __( 'Settings for mobile header.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'mobile_header__padding',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Padding', 'mpcth' ),
			'subtitle' => __( 'Specify padding for header.', 'mpcth' ),
			'compiler' => array( '.mpcth_mobile .mpcth_page__header' ),
		),

		//----------------------------------------------------------------------------//
		//	TYPOGRAPHY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'mobile_header__font-start',
			'type'     => 'section',
			'title'    => __( 'Typography', 'mpcth' ),
			'subtitle' => __( 'Specify font setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'             => 'mobile_header__font-content',
			'type'           => 'typography',
			'title'          => __( 'Content', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify content font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_mobile .mpcth_page__header' ),
		),
		array(
			'id'       => 'mobile_header__font-content--alt',
			'type'     => 'color',
			'title'    => __( 'Alt Content Color', 'mpcth' ),
			'subtitle' => __( 'Specify alternative header font color.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( '.mpcth_mobile .mpcth_page__header.mpcth_alt' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'          => 'mobile_header__font-color',
			'type'        => 'link_color',
			'title'       => __( 'Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_mobile .mpcth_page__header a' ),
		),
		array(
			'id'          => 'mobile_header__font-color--alt',
			'type'        => 'link_color',
			'title'       => __( 'Alt Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors in alternative header.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_mobile .mpcth_page__header.mpcth_alt a' ),
		),
		array(
			'id'     => 'mobile_header__font-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'mobile_header__background-start',
			'type'     => 'section',
			'title'    => __( 'Background', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'mobile_header__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'mobile_header__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#fafafa',
			),
			'compiler' => array( '.mpcth_mobile .mpcth_page__header.mpcth_background--default .mpcth_background-target' ),
			'required' => array( 'header__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'mobile_header__background-gradient',
			'type'     => 'mpc_gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_mobile .mpcth_page__header.mpcth_background--gradient .mpcth_background-target' ),
			'required' => array( 'header__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'mobile_header__background-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND - ALTERNATIVE
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'mobile_header_alt__background-start',
			'type'     => 'section',
			'title'    => __( 'Background - Alt', 'mpcth' ),
			'subtitle' => __( 'Specify background setting for alternative header.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'mobile_header_alt__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'mobile_header_alt__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#fafafa',
			),
			'compiler' => array( '.mpcth_mobile .mpcth_page__header.mpcth_alt.mpcth_background--default .mpcth_background-target' ),
			'required' => array( 'mobile_header_alt__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'mobile_header_alt__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_mobile .mpcth_page__header.mpcth_alt.mpcth_background--gradient .mpcth_background-target' ),
			'required' => array( 'mobile_header_alt__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'mobile_header_alt__background-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
