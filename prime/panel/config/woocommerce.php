<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: WOOCOMMERCE ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'  => __( 'WooCommerce', 'mpcth' ),
	'id'     => 'wc__section',
	'desc'   => __( 'Settings for WooCommerce.', 'mpcth' ),
	'icon'   => 'el el-shopping-cart',
	'fields' => array(
		//----------------------------------------------------------------------------//
		//	BADGES
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'wc_badge-start',
			'type'     => 'section',
			'title'    => __( 'Badges', 'mpcth' ),
			'subtitle' => __( 'Specify badges settings.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'wc_badge__new-days',
			'type'     => 'spinner',
			'title'    => __( 'New Label Duration', 'mpcth' ),
			'subtitle' => __( 'Specify how long in days to display \'New\' label on items (set 0 to disable).', 'mpcth' ),
			'default'  => '30',
			'min'      => '0',
			'step'     => '1',
			'max'      => '365',
		),
		array(
			'id'             => 'wc_badge__font',
			'type'           => 'typography',
			'title'          => __( 'Typography', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify badge font settings.', 'mpcth' ),
			'line-height'    => false,
			'color'          => false,
			'text-align'     => false,
			'compiler'       => array( '.woocommerce .product.product .onsale, .woocommerce .mpcth_badge--new' ),
		),
		array(
			'id'       => 'wc_badge__new-color',
			'type'     => 'color',
			'title'    => __( 'New Badge Font Color', 'mpcth' ),
			'subtitle' => __( 'Specify \'New\' badge font.', 'mpcth' ),
			'default'  => '#ffffff',
			'validate' => 'color',
			'compiler' => array( '.woocommerce .mpcth_badge--new' ),
		),
		array(
			'id'       => 'wc_badge__sale-color',
			'type'     => 'color',
			'title'    => __( 'Sale Badge Font Color', 'mpcth' ),
			'subtitle' => __( 'Specify \'Sale\' badge font.', 'mpcth' ),
			'default'  => '#ffffff',
			'validate' => 'color',
			'compiler' => array( '.woocommerce .product.product .onsale' ),
		),
		array(
			'id'       => 'wc_badge__new-background',
			'type'     => 'color',
			'title'    => __( 'New Badge Background Color', 'mpcth' ),
			'subtitle' => __( 'Specify \'New\' badge background.', 'mpcth' ),
			'default'  => '#333333',
			'validate' => 'color',
			'compiler' => array( 'background' => '.woocommerce .mpcth_badge--new' ),
		),
		array(
			'id'       => 'wc_badge__sale-background',
			'type'     => 'color',
			'title'    => __( 'Sale Badge Background Color', 'mpcth' ),
			'subtitle' => __( 'Specify \'Sale\' badge background.', 'mpcth' ),
			'default'  => '#1ea7f5',
			'validate' => 'color',
			'compiler' => array( 'background' => '.woocommerce .product.product .onsale' ),
		),
		array(
			'id'     => 'wc_badge-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
