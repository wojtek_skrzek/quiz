<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: BLOG ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'  => __( 'Blog', 'mpcth' ),
	'id'     => 'blog__section',
	'desc'   => __( 'Settings for blog.', 'mpcth' ),
	'icon'   => 'el el-pencil',
	'fields' => array(
		array(
			'id'       => 'blog__post-color',
			'type'     => 'color',
			'title'    => __( 'Post - Color', 'mpcth' ),
			'subtitle' => __( 'Specify post color.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( 'background' => '.mpcth_blog .mpcth_post' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'blog__gap',
			'type'     => 'spinner',
			'title'    => __( 'Gap', 'mpcth' ),
			'subtitle' => __( 'Specify posts gap.', 'mpcth' ),
			'default'  => 30,
			'min'      => 0,
			'max'      => 100,
			'compiler' => true,
		),
		array(
			'id'       => 'blog__columns',
			'type'     => 'button_set',
			'title'    => __( 'Columns', 'mpcth' ),
			'subtitle' => __( 'Specify columns number.', 'mpcth' ),
			'options'  => array(
				'1' => __( 'One', 'mpcth' ),
				'2' => __( 'Two', 'mpcth' ),
				'3' => __( 'Three', 'mpcth' ),
				'4' => __( 'Four', 'mpcth' ),
			),
			'default'  => '1',
			'compiler' => true,
		),
		array(
			'id'       => 'blog__posts-num',
			'type'     => 'spinner',
			'title'    => __( 'Posts Per Page', 'mpcth' ),
			'subtitle' => __( 'Specify number of displayed posts per page.', 'mpcth' ),
			'default'  => 10,
			'min'      => 1,
			'max'      => 100,
		),
		array(
			'id'       => 'blog__pagination',
			'type'     => 'button_set',
			'title'    => __( 'Pagination', 'mpcth' ),
			'subtitle' => __( 'Specify pagination style.', 'mpcth' ),
			'options'  => array(
				'classic'   => __( 'Classic', 'mpcth' ),
				'load_more' => __( 'Load More', 'mpcth' ),
				'infinite'  => __( 'Infinite Scroll', 'mpcth' ),
			),
			'default'  => 'classic',
		),
	),
) );
