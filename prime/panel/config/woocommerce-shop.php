<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: WOOCOMMERCE - SHOP ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Shop', 'mpcth' ),
	'id'         => 'wc_shop__section',
	'desc'       => __( 'Settings for WooCommerce Shop.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'wc_shop__gap',
			'type'     => 'spinner',
			'title'    => __( 'Gap', 'mpcth' ),
			'subtitle' => __( 'Specify products gap.', 'mpcth' ),
			'default'  => 30,
			'min'      => 0,
			'max'      => 100,
			'compiler' => true,
		),
		array(
			'id'       => 'wc_shop__columns',
			'type'     => 'button_set',
			'title'    => __( 'Columns', 'mpcth' ),
			'subtitle' => __( 'Specify columns number.', 'mpcth' ),
			'options'  => array(
				'1' => __( 'One', 'mpcth' ),
				'2' => __( 'Two', 'mpcth' ),
				'3' => __( 'Three', 'mpcth' ),
				'4' => __( 'Four', 'mpcth' ),
			),
			'default'  => '1',
			'compiler' => true,
		),
		array(
			'id'       => 'wc_shop__posts-num',
			'type'     => 'spinner',
			'title'    => __( 'Products Per Page', 'mpcth' ),
			'subtitle' => __( 'Specify number of displayed products per page.', 'mpcth' ),
			'default'  => 10,
			'min'      => 1,
			'max'      => 100,
		),

		//----------------------------------------------------------------------------//
		//	PRODUCT
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'wc_shop_product-start',
			'type'     => 'section',
			'title'    => __( 'Product', 'mpcth' ),
			'subtitle' => __( 'Specify product settings.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'             => 'wc_shop_product__font-title',
			'type'           => 'typography',
			'title'          => __( 'Title', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'text-align'     => false,
			'subtitle'       => __( 'Specify title font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_body.woocommerce .products .product h3' ),
		),
		array(
			'id'             => 'wc_shop_product__font-price',
			'type'           => 'typography',
			'title'          => __( 'Price', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'text-align'     => false,
			'subtitle'       => __( 'Specify price font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_body.woocommerce .products .product .price' ),
		),
		array(
			'id'       => 'wc_shop_product__rating-size',
			'type'     => 'spinner',
			'title'    => __( 'Rating Size', 'mpcth' ),
			'subtitle' => __( 'Specify rating size (set 0 to disable).', 'mpcth' ),
			'default'  => '16',
			'min'      => '0',
			'step'     => '1',
			'max'      => '50',
			'compiler' => true,
		),
		array(
			'id'       => 'wc_shop_product__rating-color',
			'type'     => 'color',
			'title'    => __( 'Rating Color', 'mpcth' ),
			'subtitle' => __( 'Specify rating font color.', 'mpcth' ),
			'default'  => '#1ea7f5',
			'validate' => 'color',
			'compiler' => array( '.mpcth_body.woocommerce .products .product .star-rating' ),
		),
		array(
			'id'       => 'wc_shop_product__align',
			'type'     => 'button_set',
			'title'    => __( 'Text Alignment', 'mpcth' ),
			'subtitle' => __( 'Specify text alignment.', 'mpcth' ),
			'options'  => array(
				'left'   => __( 'Left', 'mpcth' ),
				'center' => __( 'Center', 'mpcth' ),
				'right'  => __( 'Right', 'mpcth' ),
			),
			'default'  => 'center',
			'compiler' => true,
		),
		array(
			'id'       => 'wc_shop_product__background',
			'type'     => 'background',
			'title'    => __( 'Background', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#f9f9f9',
			),
			'compiler' => array( '.woocommerce .products .product .mpcth_product-wrapper' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'     => 'wc_shop_product-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	ADD TO CART
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'wc_shop_add_to_cart-start',
			'type'     => 'section',
			'title'    => __( 'Add to Cart', 'mpcth' ),
			'subtitle' => __( 'Specify \'Add to Cart\' settings.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'             => 'wc_shop_add_to_cart__font',
			'type'           => 'typography',
			'title'          => __( 'Typography', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'text-align'     => false,
			'color'          => false,
			'subtitle'       => __( 'Specify button font settings.', 'mpcth' ),
			'compiler'       => array( '.woocommerce .products .add_to_cart_button' ),
		),
		array(
			'id'       => 'wc_shop_add_to_cart__color',
			'type'     => 'color',
			'title'    => __( 'Font Color', 'mpcth' ),
			'subtitle' => __( 'Specify font color.', 'mpcth' ),
			'default'  => '#333333',
			'validate' => 'color',
			'compiler' => array( '.woocommerce .products .add_to_cart_button' ),
		),
		array(
			'id'       => 'wc_shop_add_to_cart__color--hover',
			'type'     => 'color',
			'title'    => __( 'Active Font Color', 'mpcth' ),
			'subtitle' => __( 'Specify active font color.', 'mpcth' ),
			'default'  => '#ffffff',
			'validate' => 'color',
			'compiler' => array( '.woocommerce .products .add_to_cart_button:hover' ),
		),
		array(
			'id'       => 'wc_shop_add_to_cart__background',
			'type'     => 'color',
			'title'    => __( 'Background Color', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#f3f3f3',
			'validate' => 'color',
			'compiler' => array( 'background' => '.woocommerce .products .add_to_cart_button' ),
		),
		array(
			'id'       => 'wc_shop_add_to_cart__background--hover',
			'type'     => 'color',
			'title'    => __( 'Active Background Color', 'mpcth' ),
			'subtitle' => __( 'Specify active background.', 'mpcth' ),
			'default'  => '#1ea7f5',
			'validate' => 'color',
			'compiler' => array( 'background' => '.woocommerce .products .add_to_cart_button:hover' ),
		),
		array(
			'id'     => 'wc_shop_add_to_cart-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
