<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: WOOCOMMERCE - PRODUCT ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Product', 'mpcth' ),
	'id'         => 'wc_product__section',
	'desc'       => __( 'Settings for WooCommerce Product.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'             => 'wc_product__font-title',
			'type'           => 'typography',
			'title'          => __( 'Title', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'text-align'     => false,
			'subtitle'       => __( 'Specify title font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_body.woocommerce.single-product .product h1' ),
		),
		array(
			'id'             => 'wc_product__font-price',
			'type'           => 'typography',
			'title'          => __( 'Price', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'text-align'     => false,
			'subtitle'       => __( 'Specify price font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_body.woocommerce.single-product .product .price' ),
		),
		array(
			'id'       => 'wc_product__rating-size',
			'type'     => 'spinner',
			'title'    => __( 'Rating Size', 'mpcth' ),
			'subtitle' => __( 'Specify rating size.', 'mpcth' ),
			'default'  => '16',
			'min'      => '0',
			'step'     => '1',
			'max'      => '50',
			'compiler' => true,
		),
		array(
			'id'       => 'wc_product__rating-color',
			'type'     => 'color',
			'title'    => __( 'Rating Color', 'mpcth' ),
			'subtitle' => __( 'Specify rating font color.', 'mpcth' ),
			'default'  => '#1ea7f5',
			'validate' => 'color',
			'compiler' => array( '.mpcth_body.woocommerce.single-product .product .star-rating' ),
		),
	),
) );
