<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: INPUTS ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'  => __( 'Inputs', 'mpcth' ),
	'id'     => 'inputs__section',
	'desc'   => __( 'Settings for inputs.', 'mpcth' ),
	'subsection' => true,
	'fields' => array(
		//----------------------------------------------------------------------------//
		//	INPUTS
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'inputs-start',
			'type'     => 'section',
			'title'    => __( 'Inputs', 'mpcth' ),
			'subtitle' => __( 'Specify inputs settings.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'inputs__border',
			'type'     => 'border',
			'title'    => __( 'Border', 'mpcth' ),
			'subtitle' => __( 'Specify inputs border.', 'mpcth' ),
			'all'      => false,
			'default'  => array(
				'border-color'  => '#eeeeee',
				'border-style'  => 'solid',
				'border-top'    => '1',
				'border-right'  => '1',
				'border-bottom' => '1',
				'border-left'   => '1',
			),
			'compiler' => array( '.mpcth_body #mpcth_page .input-text, .mpcth_body #mpcth_page .orderby, .mpcth_body #mpcth_page .select2-choice, .mpcth_body .comment-form input:not([type=submit]), .mpcth_body .comment-form textarea' ),
		),
		array(
			'id'       => 'inputs__background',
			'type'     => 'color',
			'title'    => __( 'Default Background Color', 'mpcth' ),
			'subtitle' => __( 'Specify default inputs background.', 'mpcth' ),
			'default'  => '#f3f3f3',
			'validate' => 'color',
			'compiler' => array( 'background' => '.mpcth_body #mpcth_page .input-text, .mpcth_body #mpcth_page .orderby, .mpcth_body #mpcth_page .select2-choice, .mpcth_body .comment-form input:not([type=submit]), .mpcth_body .comment-form textarea', ),
		),
		array(
			'id'       => 'inputs__background--hover',
			'type'     => 'color',
			'title'    => __( 'Active Background Color', 'mpcth' ),
			'subtitle' => __( 'Specify active inputs background.', 'mpcth' ),
			'default'  => '#ffffff',
			'validate' => 'color',
			'compiler' => array( 'background' => '.mpcth_body #mpcth_page .input-text:hover, .mpcth_body #mpcth_page .orderby:hover, .mpcth_body #mpcth_page .select2-choice:hover, .mpcth_body .comment-form input:not([type=submit]):hover, .mpcth_body .comment-form textarea:hover, .mpcth_body #mpcth_page .input-text:focus, .mpcth_body #mpcth_page .orderby:focus, .mpcth_body #mpcth_page .select2-choice:focus, .mpcth_body .comment-form input:not([type=submit]):focus, .mpcth_body .comment-form textarea:focus', ),
		),
		array(
			'id'       => 'inputs__padding',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Padding', 'mpcth' ),
			'subtitle' => __( 'Specify padding for inputs.', 'mpcth' ),
			'default'  => array(
				'padding-top'     => '.5em',
				'padding-right'   => '.5em',
				'padding-bottom'  => '.5em',
				'padding-left'    => '.5em',
				'units'           => 'em',
			),
			'compiler' => array( '.mpcth_body #mpcth_page .input-text, .mpcth_body #mpcth_page .orderby, .mpcth_body #mpcth_page .select2-choice, .mpcth_body .comment-form input:not([type=submit]), .mpcth_body .comment-form textarea' ),
		),
		array(
			'id'     => 'inputs-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BUTTONS
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'buttons-start',
			'type'     => 'section',
			'title'    => __( 'Buttons', 'mpcth' ),
			'subtitle' => __( 'Specify buttons settings.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'buttons__color',
			'type'     => 'color',
			'title'    => __( 'Default Font Color', 'mpcth' ),
			'subtitle' => __( 'Specify default button font.', 'mpcth' ),
			'default'  => '#333333',
			'validate' => 'color',
			'compiler' => array( '.mpcth_body #respond input#submit, .mpcth_body a.button, .mpcth_body button.button, .mpcth_body input.button, .mpcth_body #respond input#submit.alt:hover, .mpcth_body a.button.alt:hover, .mpcth_body button.button.alt:hover, .mpcth_body input.button.alt:hover, .mpcth_body .comment-form input[type=submit], .mpc-button, .mpc-button.alt:hover', ),
		),
		array(
			'id'       => 'buttons__background',
			'type'     => 'color',
			'title'    => __( 'Default Background Color', 'mpcth' ),
			'subtitle' => __( 'Specify default button background.', 'mpcth' ),
			'default'  => '#f3f3f3',
			'validate' => 'color',
			'compiler' => array( 'background' => '.mpcth_body #respond input#submit, .mpcth_body a.button, .mpcth_body button.button, .mpcth_body input.button, .mpcth_body #respond input#submit.alt:hover, .mpcth_body a.button.alt:hover, .mpcth_body button.button.alt:hover, .mpcth_body input.button.alt:hover, .mpcth_body .comment-form input[type=submit], .mpc-button, .mpc-button.alt:hover', ),
		),
		array(
			'id'       => 'buttons__border',
			'type'     => 'border',
			'title'    => __( 'Default Border', 'mpcth' ),
			'subtitle' => __( 'Specify default button border.', 'mpcth' ),
			'all'      => false,
			'default'  => array(
				'border-color'  => '#333333',
				'border-style'  => 'solid',
				'border-top'    => '2',
				'border-right'  => '2',
				'border-bottom' => '2',
				'border-left'   => '2',
			),
			'compiler' => array( '.mpcth_body #respond input#submit, .mpcth_body a.button, .mpcth_body button.button, .mpcth_body input.button, .mpcth_body #respond input#submit.alt:hover, .mpcth_body a.button.alt:hover, .mpcth_body button.button.alt:hover, .mpcth_body input.button.alt:hover, .mpcth_body .comment-form input[type=submit], .mpc-button, .mpc-button.alt:hover' ),
		),
		array(
			'id'       => 'buttons__color--hover',
			'type'     => 'color',
			'title'    => __( 'Active Font Color', 'mpcth' ),
			'subtitle' => __( 'Specify active button font.', 'mpcth' ),
			'default'  => '#ffffff',
			'validate' => 'color',
			'compiler' => array( '.mpcth_body #respond input#submit:hover, .mpcth_body a.button:hover, .mpcth_body button.button:hover, .mpcth_body input.button:hover, .mpcth_body #respond input#submit.alt, .mpcth_body a.button.alt, .mpcth_body button.button.alt, .mpcth_body input.button.alt, .mpcth_body .comment-form input[type=submit]:hover, .mpc-button:hover, .mpc-button.alt', ),
		),
		array(
			'id'       => 'buttons__background--hover',
			'type'     => 'color',
			'title'    => __( 'Active Background Color', 'mpcth' ),
			'subtitle' => __( 'Specify active button background.', 'mpcth' ),
			'default'  => '#1ea7f5',
			'validate' => 'color',
			'compiler' => array( 'background' => '.mpcth_body #respond input#submit:hover, .mpcth_body a.button:hover, .mpcth_body button.button:hover, .mpcth_body input.button:hover, .mpcth_body #respond input#submit.alt, .mpcth_body a.button.alt, .mpcth_body button.button.alt, .mpcth_body input.button.alt, .mpcth_body .comment-form input[type=submit]:hover, .mpc-button:hover, .mpc-button.alt', ),
		),
		array(
			'id'       => 'buttons__border--hover',
			'type'     => 'border',
			'title'    => __( 'Active Border', 'mpcth' ),
			'subtitle' => __( 'Specify active button border.', 'mpcth' ),
			'all'      => false,
			'default'  => array(
				'border-color'  => '#1ea7f5',
				'border-style'  => 'solid',
				'border-top'    => '2',
				'border-right'  => '2',
				'border-bottom' => '2',
				'border-left'   => '2',
			),
			'compiler' => array( '.mpcth_body #respond input#submit:hover, .mpcth_body a.button:hover, .mpcth_body button.button:hover, .mpcth_body input.button:hover, .mpcth_body #respond input#submit.alt, .mpcth_body a.button.alt, .mpcth_body button.button.alt, .mpcth_body input.button.alt, .mpcth_body .comment-form input[type=submit]:hover, .mpc-button:hover, .mpc-button.alt' ),
		),
		array(
			'id'       => 'buttons__padding',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Padding', 'mpcth' ),
			'subtitle' => __( 'Specify padding for button.', 'mpcth' ),
			'default'  => array(
				'padding-top'     => '.5em',
				'padding-right'   => '.5em',
				'padding-bottom'  => '.5em',
				'padding-left'    => '.5em',
				'units'           => 'em',
			),
			'compiler' => array( '.mpcth_body #mpcth_page .button, .mpcth_body .comment-form input[type=submit], .mpc-button' ),
		),
		array(
			'id'     => 'buttons-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
