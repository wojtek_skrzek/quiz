<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: FOOTER - COPYRIGHTS ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Copyrights', 'mpcth' ),
	'id'         => 'copyrights__section',
	'desc'       => __( 'Settings for copyrights.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'copyrights__disable',
			'type'     => 'switch',
			'title'    => __( 'Disable Copyrights', 'mpcth' ),
			'subtitle' => __( 'Switch to disable copyrights.', 'mpcth' ),
			'default'  => false,
		),

		array(
			'id'       => 'copyrights__content',
			'type'     => 'text',
			'title'    => __( 'Text', 'mpcth' ),
			'subtitle' => __( 'Specify copyrights text.', 'mpcth' ),
			'default'  => __( '&copy; 2015 Massive Pixel Creation. All rights reserved.', 'mpcth' ),
			'required' => array( 'copyrights__disable', '=', false ),
		),
		array(
			'id'       => 'copyrights__content-align',
			'type'     => 'button_set',
			'title'    => __( 'Text Position', 'mpcth' ),
			'subtitle' => __( 'Specify copyrights position.', 'mpcth' ),
			'options'  => array(
				'left'   => __( 'Left', 'mpcth' ),
				'center' => __( 'Center', 'mpcth' ),
				'right'  => __( 'Right', 'mpcth' ),
				'none'   => __( 'None', 'mpcth' ),
			),
			'default'  => 'center',
			'required' => array( 'copyrights__disable', '=', false ),
		),
		array(
			'id'       => 'copyrights__socials-align',
			'type'     => 'button_set',
			'title'    => __( 'Socials Position', 'mpcth' ),
			'subtitle' => __( 'Specify socials position.', 'mpcth' ),
			'options'  => array(
				'left'   => __( 'Left', 'mpcth' ),
				'center' => __( 'Center', 'mpcth' ),
				'right'  => __( 'Right', 'mpcth' ),
				'none'   => __( 'None', 'mpcth' ),
			),
			'default'  => 'none',
			'required' => array( 'copyrights__disable', '=', false ),
		),
		array(
			'id'       => 'copyrights__stretch',
			'type'     => 'switch',
			'title'    => __( 'Enable Stretch', 'mpcth' ),
			'subtitle' => __( 'Switch to enable fullwidth copyrights stretch.', 'mpcth' ),
			'default'  => false,
			'required' => array( 'copyrights__disable', '=', false ),
		),
		array(
			'id'       => 'copyrights__padding',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Padding', 'mpcth' ),
			'subtitle' => __( 'Specify padding for copyrights.', 'mpcth' ),
			'compiler' => array( '.mpcth_copyrights-wrapper' ),
			'required' => array( 'copyrights__disable', '=', false ),
		),

		//----------------------------------------------------------------------------//
		//	TYPOGRAPHY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'copyrights__font-start',
			'type'     => 'section',
			'title'    => __( 'Typography', 'mpcth' ),
			'subtitle' => __( 'Specify font setting.', 'mpcth' ),
			'indent'   => true,
			'required' => array( 'copyrights__disable', '=', false ),
		),
		array(
			'id'             => 'copyrights__font-content',
			'type'           => 'typography',
			'title'          => __( 'Content', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'text-align'     => false,
			'subtitle'       => __( 'Specify content font settings.', 'mpcth' ),
			'default'        => array(
				'color'          => '#ffffff',
			),
			'compiler'       => array( '.mpcth_copyrights', '.mpcth_copyrights a' ),
		),
		array(
			'id'       => 'copyrights__font-links',
			'type'     => 'link_color',
			'title'    => __( 'Links - Colors', 'mpcth' ),
			'subtitle' => __( 'Specify links colors.', 'mpcth' ),
			'active'   => false,
			'default'  => array(
				'regular' => '#ffffff',
				'hover'   => '#1ea7f5',
			),
			'compiler' => array( '.mpcth_copyrights a' ),
		),
		array(
			'id'     => 'copyrights__font-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'copyrights__background-start',
			'type'     => 'section',
			'title'    => __( 'Background', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
			'required' => array( 'copyrights__disable', '=', false ),
		),
		array(
			'id'       => 'copyrights__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'copyrights__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#333333',
			),
			'compiler' => array( '.mpcth_copyrights-wrapper.mpcth_background--default' ),
			'required' => array( 'copyrights__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'copyrights__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_copyrights-wrapper.mpcth_background--gradient' ),
			'required' => array( 'copyrights__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'copyrights__background-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
