<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: SOCIALS ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Socials', 'mpcth' ),
	'desc'       => __( 'All available social options.', 'mpcth' ),
	'id'         => 'socials',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'socials__list',
			'type'     => 'socials',
			'title'    => __( 'Socials List', 'mpcth' ),
			'subtitle' => __( 'Specify your socials list.', 'mpcth' ),
		),
		array(
			'id'       => 'socials__size',
			'type'     => 'spinner',
			'title'    => __( 'Size', 'mpcth' ),
			'subtitle' => __( 'Specify social icons size. Leave empty to inherit from content font size.', 'mpcth' ),
			'default'  => 18,
			'min'      => 0,
			'max'      => 100,
			'compiler' => true,
		),
	),
) );
