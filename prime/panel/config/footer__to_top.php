<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: FOOTER - BACK TO TOP ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Back To Top', 'mpcth' ),
	'id'         => 'to_top__section',
	'desc'       => __( 'Settings for "Back To Top" button.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'to_top__disable',
			'type'     => 'switch',
			'title'    => __( 'Disable "Back To Top"', 'mpcth' ),
			'subtitle' => __( 'Switch to disable button.', 'mpcth' ),
			'default'  => false,
		),
		array(
			'id'       => 'to_top__position',
			'type'     => 'button_set',
			'title'    => __( 'Position', 'mpcth' ),
			'subtitle' => __( 'Specify button position.', 'mpcth' ),
			'options'  => array(
				'left'    => __( 'Left', 'mpcth' ),
				'center'  => __( 'Center', 'mpcth' ),
				'right'   => __( 'Right', 'mpcth' ),
			),
			'default'  => 'right',
			'required' => array( 'to_top__disable', '=', false ),
		),
		array(
			'id'       => 'to_top__color',
			'type'     => 'color',
			'title'    => __( 'Color', 'mpcth' ),
			'subtitle' => __( 'Specify button color.', 'mpcth' ),
			'default'  => '#333333',
			'validate' => 'color',
			'compiler' => array( '.mpcth_back-to-top' ),
			'required' => array( 'to_top__disable', '=', false ),
		),
		array(
			'id'       => 'to_top__padding',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Padding', 'mpcth' ),
			'subtitle' => __( 'Specify button padding.', 'mpcth' ),
			'compiler' => array( '.mpcth_back-to-top' ),
			'required' => array( 'to_top__disable', '=', false ),
		),
		array(
			'id'       => 'to_top__margin',
			'type'     => 'spacing',
			'mode'     => 'margin',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Margin', 'mpcth' ),
			'subtitle' => __( 'Specify button margin.', 'mpcth' ),
			'compiler' => array( '.mpcth_back-to-top' ),
			'required' => array( 'to_top__disable', '=', false ),
		),
		array(
			'id'       => 'to_top__border',
			'type'     => 'border',
			'title'    => __( 'Border', 'mpcth' ),
			'subtitle' => __( 'Specify button border.', 'mpcth' ),
			'all'      => false,
			'compiler' => array( '.mpcth_back-to-top' ),
			'required' => array( 'to_top__disable', '=', false ),
		),
		array(
			'id'       => 'to_top__radius',
			'type'     => 'spinner',
			'title'    => __( 'Border Radius', 'mpcth' ),
			'subtitle' => __( 'Specify button border radius.', 'mpcth' ),
			'default'  => '5',
			'min'      => '0',
			'step'     => '1',
			'max'      => '100',
			'compiler' => true,
			'required' => array( 'to_top__disable', '=', false ),
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'to_top__background-start',
			'type'     => 'section',
			'title'    => __( 'Background', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
			'required' => array( 'to_top__disable', '=', false ),
		),
		array(
			'id'       => 'to_top__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'to_top__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#f3f3f3',
			),
			'compiler' => array( '.mpcth_back-to-top.mpcth_background--default' ),
			'required' => array( 'copyrights__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'to_top__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_back-to-top.mpcth_background--gradient' ),
			'required' => array( 'copyrights__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'to_top__background-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
