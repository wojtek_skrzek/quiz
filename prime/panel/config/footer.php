<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: FOOTER ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'  => __( 'Footer', 'mpcth' ),
	'id'     => 'footer__section',
	'desc'   => __( 'Settings for footer.', 'mpcth' ),
	'icon'   => 'el el-website',
	'fields' => array(
		array(
			'id'       => 'footer__display',
			'type'     => 'button_set',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify footer style.', 'mpcth' ),
			'options'  => array(
				'classic' => __( 'Classic', 'mpcth' ),
				'fixed'   => __( 'Fixed', 'mpcth' ),
			),
			'default'  => 'classic',
		),

		//----------------------------------------------------------------------------//
		//	DIVIDER
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'footer__divider-start',
			'type'     => 'section',
			'title'    => __( 'Divider', 'mpcth' ),
			'subtitle' => __( 'Specify footer top divider.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'footer__divider-width',
			'type'     => 'spinner',
			'title'    => __( 'Width', 'mpcth' ),
			'subtitle' => __( 'Specify divider width. Set 0 (zero) to disable.', 'mpcth' ),
			'default'  => 0,
			'min'      => 0,
			'max'      => 10,
		),
		array(
			'id'       => 'footer__divider-color',
			'type'     => 'color',
			'title'    => __( 'Color', 'mpcth' ),
			'subtitle' => __( 'Specify divider color.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( 'border-top-color' => '.mpcth_page__footer' ),
			'class'    => 'mpc-color-picker',
			'required' => array( 'footer__divider-width', '!=', 0 ),
		),
		array(
			'id'     => 'footer__divider-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	TYPOGRAPHY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'footer__font-start',
			'type'     => 'section',
			'title'    => __( 'Typography', 'mpcth' ),
			'subtitle' => __( 'Specify font setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'             => 'footer__font-content',
			'type'           => 'typography',
			'title'          => __( 'Content', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify content font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_page__footer' ),
		),
		array(
			'id'          => 'footer__font-links',
			'type'        => 'link_color',
			'title'       => __( 'Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_page__footer a' ),
		),
		array(
			'id'     => 'footer__font-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'footer__background-start',
			'type'     => 'section',
			'title'    => __( 'Background', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'footer__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'footer__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#f7f7f7',
			),
			'compiler' => array( '.mpcth_page__footer.mpcth_background--default' ),
			'required' => array( 'footer__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'footer__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_page__footer.mpcth_background--gradient' ),
			'required' => array( 'footer__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'footer__background-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
