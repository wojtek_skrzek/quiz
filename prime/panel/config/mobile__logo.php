<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: MOBILE - LOGO ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Logo', 'mpcth' ),
	'id'         => 'mobile_logo__section',
	'desc'       => __( 'Settings for mobile logo.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'mobile_logo__type',
			'type'     => 'button_set',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify logo type.', 'mpcth' ),
			'options'  => array(
				'text'  => __( 'Text', 'mpcth' ),
				'image' => __( 'Image', 'mpcth' ),
			),
			'default'  => 'text',
		),
		array(
			'id'       => 'mobile_logo__text',
			'type'     => 'text',
			'title'    => __( 'Text', 'mpcth' ),
			'subtitle' => __( 'Specify logo text.', 'mpcth' ),
			'default'  => __( 'Logo', 'mpcth' ),
			'required' => array( 'mobile_logo__type', '=', 'text' ),
		),
		array(
			'id'             => 'mobile_logo__text-font',
			'type'           => 'typography',
			'title'          => __( 'Text - Font', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify logo font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_page__header .mpcth_logo.mpcth_mobile a' ),
			'required'       => array( 'mobile_logo__type', '=', 'text' ),
		),
		array(
			'id'       => 'mobile_logo__text-font--alt',
			'type'     => 'color',
			'title'    => __( 'Text - Alt Color', 'mpcth' ),
			'subtitle' => __( 'Specify logo font color for alternative header.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( '.mpcth_alt:not(.mpcth_floating) .mpcth_logo.mpcth_mobile a' ),
			'class'    => 'mpc-color-picker',
			'required' => array( 'mobile_logo__type', '=', 'text' ),
		),
		array(
			'id'       => 'mobile_logo__image',
			'type'     => 'media',
			'title'    => __( 'Image', 'mpcth' ),
			'subtitle' => __( 'Specify logo image.', 'mpcth' ),
			'required' => array( 'mobile_logo__type', '=', 'image' ),
		),
		array(
			'id'       => 'mobile_logo__image--alt',
			'type'     => 'media',
			'title'    => __( 'Alt Image', 'mpcth' ),
			'subtitle' => __( 'Specify logo image for alternative header.', 'mpcth' ),
			'required' => array( 'mobile_logo__type', '=', 'image' ),
		),
		array(
			'id'       => 'mobile_logo__image-height',
			'type'     => 'dimensions',
			'title'    => __( 'Image - Height', 'mpcth' ),
			'subtitle' => __( 'Specify logo image height.', 'mpcth' ),
			'default'  => array(
				'height' => '45',
			),
			'width'    => false,
			'units'    => false,
			'required' => array( 'mobile_logo__type', '=', 'image' ),
		),
		array(
			'id'       => 'mobile_logo__padding',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Padding', 'mpcth' ),
			'subtitle' => __( 'Specify padding for logo.', 'mpcth' ),
			'compiler' => array( '.mpcth_logo.mpcth_mobile' ),
		),
	),
) );
