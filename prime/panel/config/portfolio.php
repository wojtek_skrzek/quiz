<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: PORTFOLIO ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'  => __( 'Portfolio', 'mpcth' ),
	'id'     => 'portfolio__section',
	'desc'   => __( 'Settings for portfolio.', 'mpcth' ),
	'icon'   => 'el el-bulb',
	'fields' => array(
		array(
			'id'       => 'portfolio__post-color',
			'type'     => 'color',
			'title'    => __( 'Post - Color', 'mpcth' ),
			'subtitle' => __( 'Specify post color.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( 'background' => '.mpcth_portfolio .mpcth_post' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'portfolio__gap',
			'type'     => 'spinner',
			'title'    => __( 'Gap', 'mpcth' ),
			'subtitle' => __( 'Specify posts gap.', 'mpcth' ),
			'default'  => 30,
			'min'      => 0,
			'max'      => 100,
			'compiler' => true,
		),
		array(
			'id'       => 'portfolio__columns',
			'type'     => 'button_set',
			'title'    => __( 'Columns', 'mpcth' ),
			'subtitle' => __( 'Specify columns number.', 'mpcth' ),
			'options'  => array(
				'1' => __( 'One', 'mpcth' ),
				'2' => __( 'Two', 'mpcth' ),
				'3' => __( 'Three', 'mpcth' ),
				'4' => __( 'Four', 'mpcth' ),
			),
			'default'  => '1',
			'compiler' => true,
		),
		array(
			'id'       => 'portfolio__posts-num',
			'type'     => 'spinner',
			'title'    => __( 'Posts Per Page', 'mpcth' ),
			'subtitle' => __( 'Specify number of displayed posts per page.', 'mpcth' ),
			'default'  => 10,
			'min'      => 1,
			'max'      => 100,
		),
		array(
			'id'       => 'portfolio__pagination',
			'type'     => 'button_set',
			'title'    => __( 'Pagination', 'mpcth' ),
			'subtitle' => __( 'Specify pagination style.', 'mpcth' ),
			'options'  => array(
				'classic'   => __( 'Classic', 'mpcth' ),
				'load_more' => __( 'Load More', 'mpcth' ),
				'infinite'  => __( 'Infinite Scroll', 'mpcth' ),
			),
			'default'  => 'classic',
		),
	),
) );
