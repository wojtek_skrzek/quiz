<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: MOBILE - MENU ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Menu', 'mpcth' ),
	'id'         => 'mobile_menu__section',
	'desc'       => __( 'Settings for mobile menu.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'mobile_menu__padding-item',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Padding - Item', 'mpcth' ),
			'subtitle' => __( 'Specify padding for menu items.', 'mpcth' ),
			'default'  => array(
				'padding-top'     => '0',
				'padding-right'   => '1.5em',
				'padding-bottom'  => '0',
				'padding-left'    => '1.5em',
				'units'           => 'em',
			),
			'compiler' => array( '.mpcth_mobile .mpcth_page__navigation .menu-item > a' ),
		),

		//----------------------------------------------------------------------------//
		//	TYPOGRAPHY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'mobile_menu__font-start',
			'type'     => 'section',
			'title'    => __( 'Typography', 'mpcth' ),
			'subtitle' => __( 'Specify font setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'             => 'mobile_menu__font-content',
			'type'           => 'typography',
			'title'          => __( 'Content', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify content font settings.', 'mpcth' ),
			'default'        => array(
				'line-height'    => '1.75',
			),
			'compiler'       => array( '.mpcth_mobile .mpcth_page__navigation', '.mpcth_mobile .mpcth_page__navigation .menu-item > a' ),
		),
		array(
			'id'             => 'mobile_menu__font-description',
			'type'           => 'typography',
			'title'          => __( 'Description', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify description font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_mobile .mpcth_page__navigation .sub-menu .mpc-menu-description', '.mpcth_mobile .mpcth_page__navigation .sub-menu .mpc-menu-custom-content' ),
		),
		array(
			'id'             => 'mobile_menu__font-icon',
			'type'           => 'typography',
			'title'          => __( 'Icon', 'mpcth' ),
			'units'          => 'px',
			'font-family'    => false,
			'font-style'     => false,
			'font-weight'    => false,
			'text-align'     => false,
			'subtitle'       => __( 'Specify mega menu section icon font settings.', 'mpcth' ),
			'default'        => array(
				'line-height' => '1.75',
			),
			'compiler'       => array( '.mpcth_mobile .menu-item .mpc-menu-icon', '.mpcth_mobile .menu-item .mpc-menu-icon-spacer' ),
		),
		array(
			'id'          => 'mobile_menu__font-color',
			'type'        => 'link_color',
			'title'       => __( 'Sub Menu Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_mobile .mpcth_page__navigation .menu-item a', '.mpcth_mobile .mpcth_page__navigation .sub-menu a', '.mpcth_mobile .mpcth_page__navigation .mpc-mega-menu-col .sub-menu a' ),
		),
		array(
			'id'     => 'mobile_menu__font-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'mobile_menu__background-start',
			'type'     => 'section',
			'title'    => __( 'Background', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'mobile_menu__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#fafafa',
			),
			'compiler' => array( '.mpcth_mobile .mpc-menu.mpcth_background--default' ),
			'required' => array( 'dropdown__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'mobile_menu__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_mobile .mpc-menu.mpcth_background--gradient' ),
			'required' => array( 'dropdown__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'mobile_menu__background-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND - SUB MENU
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'mobile_dropdown__background-start',
			'type'     => 'section',
			'title'    => __( 'Background - Sub Menu', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'mobile_dropdown__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#fafafa',
			),
			'compiler' => array( '.mpcth_mobile .mpcth_background--default .sub-menu', '.mpcth_mobile .mpcth_background--default .mpc-mega-menu-wrap' ),
			'required' => array( 'dropdown__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'mobile_dropdown__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_mobile .mpcth_background--gradient .sub-menu', '.mpcth_mobile .mpcth_background--gradient .sub-menu' ),
			'required' => array( 'dropdown__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'mobile_dropdown__background-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND - ACTIVE
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'mobile_active__background-start',
			'type'     => 'section',
			'title'    => __( 'Background - Active', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'mobile_active__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#f3f3f3',
			),
			'compiler' => array( '.mpcth_mobile .mpcth_background--default .mpc-menu a.mpcth_active', '.mpcth_mobile .mpcth_background--default .mpc-menu .menu-item > a:hover' ),
			'required' => array( 'dropdown__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'mobile_active__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_mobile .mpcth_background--gradient .mpc-menu a.mpcth_active', '.mpcth_mobile .mpcth_background--gradient .mpc-menu .menu-item > a:hover' ),
			'required' => array( 'dropdown__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'mobile_active__background-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
