<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: CUSTOM CSS/JS ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'  => __( 'Custom', 'mpcth' ),
	'id'     => 'custom__section',
	'desc'   => __( 'Custom settings.', 'mpcth' ),
	'icon'   => 'el el-magic',
	'fields' => array(
		array(
			'id'       => 'css_editor',
			'type'     => 'ace_editor',
			'title'    => __( 'Custom CSS', 'mpcth' ),
			'subtitle' => __( 'Write your custom CSS.', 'mpcth' ),
			'default'  => '',
			'mode'     => 'css',
			'theme'    => 'chrome',
		),
		array(
			'id'       => 'js_editor',
			'type'     => 'ace_editor',
			'title'    => __( 'Custom JS', 'mpcth' ),
			'subtitle' => __( 'Write your custom JS.', 'mpcth' ),
			'default'  => '',
			'mode'     => 'javascript',
			'theme'    => 'chrome',
		),
	),
) );
