<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: HEADER ]--*/
/*----------------------------------------------------------------------------*/

$mpcth_header_cart = array();
if ( class_exists( 'WooCommerce' ) ) {
	$mpcth_header_cart = array(
		'id'       => 'header__disable-cart',
		'type'     => 'switch',
		'title'    => __( 'Disable Cart', 'mpcth' ),
		'subtitle' => __( 'Switch to disable cart.', 'mpcth' ),
		'default'  => false,
	);
}

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'  => __( 'Header', 'mpcth' ),
	'id'     => 'header__section',
	'desc'   => __( 'Settings for header.', 'mpcth' ),
	'icon'   => 'el el-website',
	'fields' => array(
		array(
			'id'       => 'header__content-switch',
			'type'     => 'switch',
			'title'    => __( 'Enable Pre-header Content', 'mpcth' ),
			'subtitle' => __( 'Switch to enable pre-header content.', 'mpcth' ),
			'default'  => false,
		),
		array(
			'id'       => 'header__content',
			'type'     => 'editor',
			'title'    => __( 'Pre-header Content', 'mpcth' ),
			'subtitle' => __( 'Specify pre-header content.', 'mpcth' ),
			'default'  => '',
			'required' => array( 'header__content-switch', '=', true ),
		),
		array(
			'id'       => 'header__tagline',
			'type'     => 'switch',
			'title'    => __( 'Show Tagline', 'mpcth' ),
			'subtitle' => __( 'Switch to enable tagline.', 'mpcth' ),
			'default'  => false,
		),
		array(
			'id'       => 'header__tagline-text',
			'type'     => 'text',
			'title'    => __( 'Custom Tagline', 'mpcth' ),
			'subtitle' => __( 'Specify custom tagline. Leave empty to use default one.', 'mpcth' ),
			'default'  => '',
			'required' => array( 'header__tagline', '=', true ),
		),
		array(
			'id'       => 'header__padding',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Padding', 'mpcth' ),
			'subtitle' => __( 'Specify padding for header.', 'mpcth' ),
			'compiler' => array( '.mpcth_page__header' ),
		),

		//----------------------------------------------------------------------------//
		//	STYLE
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'header__style-start',
			'type'     => 'section',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify header style.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'header__layout',
			'type'     => 'button_set',
			'title'    => __( 'Layout', 'mpcth' ),
			'subtitle' => __( 'Specify header layout.', 'mpcth' ),
			'options'  => array(
				'layout_01' => __( 'Layout 1', 'mpcth' ),
				'layout_02' => __( 'Layout 2', 'mpcth' ),
				'layout_03' => __( 'Layout 3', 'mpcth' ),
				'layout_04' => __( 'Layout 4', 'mpcth' ),
				'layout_05' => __( 'Layout 5', 'mpcth' ),
				'layout_06' => __( 'Layout 6', 'mpcth' ),
			),
			'default'  => 'layout_01',
		),
		array(
			'id'       => 'header__sticky',
			'type'     => 'switch',
			'title'    => __( 'Enable Sticky', 'mpcth' ),
			'subtitle' => __( 'Switch to enable sticky header.', 'mpcth' ),
			'default'  => true,
		),
		array(
			'id'       => 'header__style',
			'type'     => 'button_set',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify header style.', 'mpcth' ),
			'options'  => array(
				'classic' => __( 'Classic', 'mpcth' ),
				'stretch' => __( 'Stretch', 'mpcth' ),
				'boxed'   => __( 'Boxed', 'mpcth' ),
			),
			'default'  => 'classic',
		),
		array(
			'id'       => 'header__float',
			'type'     => 'switch',
			'title'    => __( 'Enable Floating', 'mpcth' ),
			'subtitle' => __( 'Switch to enable floating header with content underneath.', 'mpcth' ),
			'default'  => false,
		),
		array(
			'id'       => 'header__offset',
			'type'     => 'spinner',
			'title'    => __( 'Top Offset', 'mpcth' ),
			'subtitle' => __( 'Specify header top offset.', 'mpcth' ),
			'default'  => 0,
			'min'      => 0,
			'max'      => 100,
			'compiler' => true,
			'required' => array( 'header__float', '=', true ),
		),
		array(
			'id'     => 'header__style-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	DISABLE ELEMENTS
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'header__disable-start',
			'type'     => 'section',
			'title'    => __( 'Elements', 'mpcth' ),
			'subtitle' => __( 'Specify header elements display.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'header__disable-search',
			'type'     => 'switch',
			'title'    => __( 'Disable Search', 'mpcth' ),
			'subtitle' => __( 'Switch to disable search.', 'mpcth' ),
			'default'  => false,
		),
		array(
			'id'       => 'header__disable-socials',
			'type'     => 'switch',
			'title'    => __( 'Disable Socials', 'mpcth' ),
			'subtitle' => __( 'Switch to disable socials.', 'mpcth' ),
			'default'  => false,
		),
		$mpcth_header_cart,
		array(
			'id'     => 'header__disable-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	DIVIDER
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'header__divider-start',
			'type'     => 'section',
			'title'    => __( 'Divider', 'mpcth' ),
			'subtitle' => __( 'Specify header bottom divider.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'header__divider-width',
			'type'     => 'spinner',
			'title'    => __( 'Width', 'mpcth' ),
			'subtitle' => __( 'Specify divider width. Set 0 (zero) to disable.', 'mpcth' ),
			'default'  => 0,
			'min'      => 0,
			'max'      => 10,
		),
		array(
			'id'       => 'header__divider-color',
			'type'     => 'color',
			'title'    => __( 'Color', 'mpcth' ),
			'subtitle' => __( 'Specify divider color.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( 'border-bottom-color' => '.mpcth_page__header' ),
			'class'    => 'mpc-color-picker',
			'required' => array( 'header__divider-width', '!=', 0 ),
		),
		array(
			'id'       => 'header__divider-color--alt',
			'type'     => 'color',
			'title'    => __( 'Alt Color', 'mpcth' ),
			'subtitle' => __( 'Specify alternative divider color.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( 'border-bottom-color' => '.mpcth_page__header.mpcth_alt:not(.mpcth_floating)' ),
			'class'    => 'mpc-color-picker',
			'required' => array( 'header__divider-width', '!=', 0 ),
		),
		array(
			'id'       => 'header__divider-color--sticky',
			'type'     => 'color',
			'title'    => __( 'Sticky Color', 'mpcth' ),
			'subtitle' => __( 'Specify sticky divider color.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( 'border-bottom-color' => '.mpcth_page__header.mpcth_floating' ),
			'class'    => 'mpc-color-picker',
			'required' => array( 'header__divider-width', '!=', 0 ),
		),
		array(
			'id'     => 'header__divider-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	TYPOGRAPHY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'header__font-start',
			'type'     => 'section',
			'title'    => __( 'Typography', 'mpcth' ),
			'subtitle' => __( 'Specify font setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'             => 'header__font-content',
			'type'           => 'typography',
			'title'          => __( 'Content', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify content font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_page__header' ),
		),
		array(
			'id'       => 'header__font-content--alt',
			'type'     => 'color',
			'title'    => __( 'Alt Content Color', 'mpcth' ),
			'subtitle' => __( 'Specify alternative header font color.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( '.mpcth_page__header.mpcth_alt:not(.mpcth_floating)' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'          => 'header__font-color',
			'type'        => 'link_color',
			'title'       => __( 'Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors.', 'mpcth' ),
			'active'      => false,
			'default'     => array(
				'regular' => '#555555',
				'hover'   => '#1ea7f5',
			),
			'compiler'    => array( '.mpcth_page__header a' ),
		),
		array(
			'id'          => 'header__font-color--alt',
			'type'        => 'link_color',
			'title'       => __( 'Alt Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors in alternative header.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_page__header.mpcth_alt a' ),
		),
		array(
			'id'          => 'header__font-color--sticky',
			'type'        => 'link_color',
			'title'       => __( 'Sticky Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors in sticky.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_page__header.mpcth_floating a' ),
		),
		array(
			'id'     => 'header__font-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'header__background-start',
			'type'     => 'section',
			'title'    => __( 'Background', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'header__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'header__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#fafafa',
			),
			'compiler' => array( '.mpcth_page__header.mpcth_background--default .mpcth_background-target' ),
			'required' => array( 'header__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'header__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_page__header.mpcth_background--gradient .mpcth_background-target' ),
			'required' => array( 'header__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'header__background-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND - ALTERNATIVE
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'header_alt__background-start',
			'type'     => 'section',
			'title'    => __( 'Background - Alt', 'mpcth' ),
			'subtitle' => __( 'Specify background setting for alternative header.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'header_alt__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'header_alt__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#fafafa',
			),
			'compiler' => array( '.mpcth_page__header.mpcth_alt.mpcth_background-alt--default .mpcth_background-target' ),
			'required' => array( 'header_alt__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'header_alt__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_page__header.mpcth_alt.mpcth_background-alt--gradient .mpcth_background-target' ),
			'required' => array( 'header_alt__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'header_alt__background-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND - STICKY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'header_sticky__background-start',
			'type'     => 'section',
			'title'    => __( 'Background - Sticky', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'header_sticky__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'header_sticky__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#fafafa',
			),
			'compiler' => array( '.mpcth_page__header.mpcth_floating.mpcth_background-sticky--default .mpcth_background-target' ),
			'required' => array( 'header_sticky__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'header_sticky__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_page__header.mpcth_floating.mpcth_background-sticky--gradient .mpcth_background-target' ),
			'required' => array( 'header_sticky__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'header_sticky__background-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
