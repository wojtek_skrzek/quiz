<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: BREADCRUMBS ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Breadcrumbs', 'mpcth' ),
	'id'         => 'breadcrumbs__section',
	'desc'       => __( 'Settings for breadcrumbs.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'breadcrumbs__before',
			'type'     => 'text',
			'title'    => __( 'Before Text', 'mpcth' ),
			'subtitle' => __( 'Specify before text.', 'mpcth' ),
			'default'  => '',
		),
		array(
			'id'       => 'breadcrumbs__after',
			'type'     => 'text',
			'title'    => __( 'After Text', 'mpcth' ),
			'subtitle' => __( 'Specify after text.', 'mpcth' ),
			'default'  => '',
		),

		//----------------------------------------------------------------------------//
		//	DIVIDER
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'breadcrumbs__divider-start',
			'type'     => 'section',
			'title'    => __( 'Divider', 'mpcth' ),
			'subtitle' => __( 'Specify divider setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'breadcrumbs__divider-style',
			'type'     => 'button_set',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify divider style.', 'mpcth' ),
			'options'  => array(
				'style_02' => '&#47;',
				'style_03' => '&#92;',
				'style_04' => '&#124;',
				'style_05' => '&#62;',
				'style_06' => '&#58;',
				'style_07' => '&#58;&#58;',
			),
			'default'  => 'style_02',
		),
		array(
			'id'       => 'breadcrumbs__divider-color',
			'type'     => 'color',
			'title'    => __( 'Color', 'mpcth' ),
			'subtitle' => __( 'Specify divider color.', 'mpcth' ),
			'default'  => '',
			'validate' => false,
			'compiler' => array( '.mpcth_breadcrumbs .mpcth_separator' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'     => 'breadcrumbs__divider-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	TYPOGRAPHY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'breadcrumbs__font-start',
			'type'     => 'section',
			'title'    => __( 'Typography', 'mpcth' ),
			'subtitle' => __( 'Specify font setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'             => 'breadcrumbs__font-content',
			'type'           => 'typography',
			'title'          => __( 'Content', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify content font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_breadcrumbs', '.mpcth_body.woocommerce-page .mpcth_breadcrumbs' ),
		),
		array(
			'id'          => 'breadcrumbs__font-color',
			'type'        => 'link_color',
			'title'       => __( 'Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_breadcrumbs a', '.mpcth_body.woocommerce-page .mpcth_breadcrumbs a' ),
		),
		array(
			'id'     => 'breadcrumbs__font-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
