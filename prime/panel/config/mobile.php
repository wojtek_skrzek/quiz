<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: MOBILE ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'  => __( 'Mobile', 'mpcth' ),
	'id'     => 'mobile__section',
	'icon'   => 'el el-laptop',
) );
