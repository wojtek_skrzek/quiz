<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: HEADER - LOGO ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Logo', 'mpcth' ),
	'id'         => 'logo__section',
	'desc'       => __( 'Settings for logo.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'logo__type',
			'type'     => 'button_set',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify logo type.', 'mpcth' ),
			'options'  => array(
				'text'  => __( 'Text', 'mpcth' ),
				'image' => __( 'Image', 'mpcth' ),
			),
			'default'  => 'text',
		),
		array(
			'id'       => 'logo__text',
			'type'     => 'text',
			'title'    => __( 'Text', 'mpcth' ),
			'subtitle' => __( 'Specify logo text.', 'mpcth' ),
			'default'  => __( 'Logo', 'mpcth' ),
			'required' => array( 'logo__type', '=', 'text' ),
		),
		array(
			'id'             => 'logo__text-font',
			'type'           => 'typography',
			'title'          => __( 'Text - Font', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify logo font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_logo-wrap .mpcth_logo a' ),
			'required'       => array( 'logo__type', '=', 'text' ),
		),
		array(
			'id'       => 'logo__text-font--alt',
			'type'     => 'color',
			'title'    => __( 'Text - Alt Color', 'mpcth' ),
			'subtitle' => __( 'Specify logo font color for alternative header.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( '.mpcth_alt:not(.mpcth_floating) .mpcth_logo a' ),
			'class'    => 'mpc-color-picker',
			'required' => array( 'logo__type', '=', 'text' ),
		),
		array(
			'id'       => 'logo__image',
			'type'     => 'media',
			'url'     => true,
			'title'    => __( 'Image', 'mpcth' ),
			'subtitle' => __( 'Specify logo image.', 'mpcth' ),
			'required' => array( 'logo__type', '=', 'image' ),
		),
		array(
			'id'       => 'logo__image--alt',
			'type'     => 'media',
			'title'    => __( 'Alt Image', 'mpcth' ),
			'subtitle' => __( 'Specify logo image for alternative header.', 'mpcth' ),
			'required' => array( 'logo__type', '=', 'image' ),
		),
		array(
			'id'       => 'logo__image-height',
			'type'     => 'dimensions',
			'title'    => __( 'Image - Height', 'mpcth' ),
			'subtitle' => __( 'Specify logo image height.', 'mpcth' ),
			'default'  => array(
				'height' => '45',
			),
			'width'    => false,
			'units'    => false,
			'required' => array( 'logo__type', '=', 'image' ),
		),
		array(
			'id'       => 'logo__padding',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Padding', 'mpcth' ),
			'subtitle' => __( 'Specify padding for logo.', 'mpcth' ),
			'compiler' => array( '.mpcth_logo' ),
		),
	)
) );
