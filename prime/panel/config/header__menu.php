<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: HEADER - MENU ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Menu', 'mpcth' ),
	'id'         => 'menu__section',
	'desc'       => __( 'Settings for menu.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'menu__padding-item',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Padding - Top Level Item', 'mpcth' ),
			'subtitle' => __( 'Specify padding for top level menu items.', 'mpcth' ),
			'default'  => array(
				'padding-top'    => '1em',
				'padding-right'  => '1em',
				'padding-bottom' => '1em',
				'padding-left'   => '1em',
				'units'          => 'em',
			),
			'compiler' => array( '.mpc-menu > .menu-item > a' ),
		),
		array(
			'id'       => 'menu__submenu-sign',
			'type'     => 'switch',
			'title'    => __( 'Show Sub Menu Sign', 'mpcth' ),
			'subtitle' => __( 'Switch to disable sub menu sign.', 'mpcth' ),
			'default'  => true,
		),
		array(
			'id'       => 'menu__line-color',
			'type'     => 'color',
			'title'    => __( 'Mega Menu Columns Separator Color', 'mpcth' ),
			'subtitle' => __( 'Specify mega menu columns divider color.', 'mpcth' ),
			'default'  => 'transparent',
			'validate' => false,
			'compiler' => array( 'background' => '.mpc-mega-menu-col:after' ),
			'class'    => 'mpc-color-picker',
		),

		//----------------------------------------------------------------------------//
		//	DIVIDER
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'menu__divider-start',
			'type'     => 'section',
			'title'    => __( 'Divider', 'mpcth' ),
			'subtitle' => __( 'Specify divider setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'menu__divider-style',
			'type'     => 'button_set',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify menu items divider style.', 'mpcth' ),
			'options'  => array(
				'style_01' => __( 'None', 'mpcth' ),
				'style_02' => '&#47;',
				'style_03' => '&#92;',
				'style_04' => '&#124;',
				'style_05' => '&#62;',
				'style_06' => '&#58;',
				'style_07' => '&#58;&#58;',
			),
			'default'  => 'style_01',
			'compiler' => true,
		),
		array(
			'id'       => 'menu__divider-color',
			'type'     => 'color',
			'title'    => __( 'Color', 'mpcth' ),
			'subtitle' => __( 'Specify menu items divider color.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( '.mpc-menu > .menu-item:before' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'menu__divider-color--alt',
			'type'     => 'color',
			'title'    => __( 'Alt Color', 'mpcth' ),
			'subtitle' => __( 'Specify menu items divider color for alternative header.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( '.mpcth_alt .mpc-menu > .menu-item:before' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'menu__divider-color--sticky',
			'type'     => 'color',
			'title'    => __( 'Sticky Color', 'mpcth' ),
			'subtitle' => __( 'Specify sticky menu items divider color.', 'mpcth' ),
			'validate' => false,
			'compiler' => array( '.mpcth_floating .mpc-menu > .menu-item:before' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'     => 'menu__divider-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	TYPOGRAPHY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'menu__font-start',
			'type'     => 'section',
			'title'    => __( 'Typography', 'mpcth' ),
			'subtitle' => __( 'Specify font setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'             => 'menu__font-content',
			'type'           => 'typography',
			'title'          => __( 'Content', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify content font settings.', 'mpcth' ),
			'default'        => array(
				'line-height'    => '1.75',
			),
			'compiler'       => array( '.mpcth_page__navigation .sub-menu', '.mpcth_page__navigation .sub-menu .menu-item > a' ),
		),
		array(
			'id'             => 'menu__font-description',
			'type'           => 'typography',
			'title'          => __( 'Description', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify description font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_page__navigation .sub-menu .mpc-menu-description', '.mpcth_page__navigation .sub-menu .mpc-menu-custom-content' ),
		),
		array(
			'id'             => 'menu__font-title',
			'type'           => 'typography',
			'title'          => __( 'Mega Menu Title', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify mega menu section title font settings.', 'mpcth' ),
			'default'        => array(
				'line-height' => '2.5',
			),
			'compiler'       => array( 'html:not(.mpcth_mobile) .mpc-mega-menu-col > a' ),
		),
		array(
			'id'             => 'menu__font-icon',
			'type'           => 'typography',
			'title'          => __( 'Icon', 'mpcth' ),
			'units'          => 'px',
			'font-family'    => false,
			'font-style'     => false,
			'font-weight'    => false,
			'text-align'     => false,
			'subtitle'       => __( 'Specify mega menu section icon font settings.', 'mpcth' ),
			'default'        => array(
				'line-height' => '1.75',
			),
			'compiler'       => array( '.menu-item .mpc-menu-icon', '.menu-item .mpc-menu-icon-spacer' ),
		),
		array(
			'id'          => 'menu__font-color',
			'type'        => 'link_color',
			'title'       => __( 'Dropdown Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_page__navigation .sub-menu a' ),
		),
		array(
			'id'          => 'menu__mega-menu-color',
			'type'        => 'link_color',
			'title'       => __( 'Mega Menu Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_page__navigation .mpc-mega-menu-col .sub-menu a' ),
		),
		array(
			'id'     => 'menu__font-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'dropdown__background-start',
			'type'     => 'section',
			'title'    => __( 'Background - Dropdown', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'dropdown__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'dropdown__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#f7f7f7',
			),
			'compiler' => array( '.mpcth_background--default .sub-menu', '.mpcth_background--default .mpc-mega-menu-wrap' ),
			'required' => array( 'dropdown__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'dropdown__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_background--gradient .sub-menu', '.mpcth_background--gradient .mpc-mega-menu-wrap' ),
			'required' => array( 'dropdown__background-type', '=', 'gradient' ),
		),
		array(
			'id'       => 'dropdown__background-item-hover',
			'type'     => 'color',
			'title'    => __( 'Item Hover', 'mpcth' ),
			'subtitle' => __( 'Specify dropdown item hover color.', 'mpcth' ),
			'default'  => '',
			'validate' => false,
			'class'    => 'mpc-color-picker',
			'compiler' => array( 'background-color' => '.menu-item .sub-menu .menu-item > a:hover' ),
		),
		array(
			'id'     => 'dropdown__background-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
