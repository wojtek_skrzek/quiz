<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: WOOCOMMERCE - PRODUCT ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Mini Cart', 'mpcth' ),
	'id'         => 'wc_cart__section',
	'desc'       => __( 'Settings for WooCommerce Mini Cart.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'wc_cart__disable-price',
			'type'     => 'switch',
			'title'    => __( 'Disable Icon Price', 'mpcth' ),
			'subtitle' => __( 'Switch to disable total cart price display.', 'mpcth' ),
			'default'  => false,
		),
		array(
			'id'       => 'wc_cart__disable-counter',
			'type'     => 'switch',
			'title'    => __( 'Disable Icon Counter', 'mpcth' ),
			'subtitle' => __( 'Switch to disable cart items counter display.', 'mpcth' ),
			'default'  => false,
		),

		//----------------------------------------------------------------------------//
		//	TYPOGRAPHY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'wc_cart__font-start',
			'type'     => 'section',
			'title'    => __( 'Typography', 'mpcth' ),
			'subtitle' => __( 'Specify font setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'             => 'wc_cart__font-content',
			'type'           => 'typography',
			'title'          => __( 'Content', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'text-align'     => false,
			'subtitle'       => __( 'Specify content font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_body .mpcth_cart' ),
		),
		array(
			'id'          => 'wc_cart__font-color',
			'type'        => 'link_color',
			'title'       => __( 'Dropdown Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_body .mpcth_cart-list a' ),
		),
		array(
			'id'     => 'wc_cart__font-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'wc_cart__background-start',
			'type'     => 'section',
			'title'    => __( 'Background', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'wc_cart__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'wc_cart__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => array(
				'background-color' => '#f7f7f7',
			),
			'compiler' => array( '.mpcth_body .mpcth_background--default.mpcth_cart' ),
			'required' => array( 'wc_cart__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'wc_cart__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_body .mpcth_background--gradient.mpcth_cart' ),
			'required' => array( 'wc_cart__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'wc_cart__background-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
