<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: SIDEBAR ]--*/
/*----------------------------------------------------------------------------*/

$mpcth_product_sidebar = array();
if ( class_exists( 'WooCommerce' ) ) {
	$mpcth_product_sidebar = array(
		'id'       => 'sidebar__position-product',
		'type'     => 'button_set',
		'title'    => __( 'Position - Single Product', 'mpcth' ),
		'subtitle' => __( 'Specify default product position.', 'mpcth' ),
		'options'  => array(
			'left'  => __( 'Left', 'mpcth' ),
			'none'  => __( 'None', 'mpcth' ),
			'right' => __( 'Right', 'mpcth' ),
		),
		'default'  => 'none',
	);
}

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'  => __( 'Sidebar', 'mpcth' ),
	'id'     => 'sidebar__section',
	'desc'   => __( 'Settings for sidebar.', 'mpcth' ),
	'icon'   => 'el el-website',
	'fields' => array(
		array(
			'id'       => 'sidebar__position',
			'type'     => 'button_set',
			'title'    => __( 'Position', 'mpcth' ),
			'subtitle' => __( 'Specify default position.', 'mpcth' ),
			'options'  => array(
				'left'  => __( 'Left', 'mpcth' ),
				'none'  => __( 'None', 'mpcth' ),
				'right' => __( 'Right', 'mpcth' ),
			),
			'default'  => 'none',
		),
		array(
			'id'       => 'sidebar__position-post',
			'type'     => 'button_set',
			'title'    => __( 'Position - Single Post', 'mpcth' ),
			'subtitle' => __( 'Specify default post position.', 'mpcth' ),
			'options'  => array(
				'left'  => __( 'Left', 'mpcth' ),
				'none'  => __( 'None', 'mpcth' ),
				'right' => __( 'Right', 'mpcth' ),
			),
			'default'  => 'none',
		),
		array(
			'id'       => 'sidebar__position-portfolio',
			'type'     => 'button_set',
			'title'    => __( 'Position - Single Portfolio', 'mpcth' ),
			'subtitle' => __( 'Specify default portfolio position.', 'mpcth' ),
			'options'  => array(
				'left'  => __( 'Left', 'mpcth' ),
				'none'  => __( 'None', 'mpcth' ),
				'right' => __( 'Right', 'mpcth' ),
			),
			'default'  => 'none',
		),
		$mpcth_product_sidebar,
		array(
			'id'       => 'sidebar__position-search',
			'type'     => 'button_set',
			'title'    => __( 'Position - Search', 'mpcth' ),
			'subtitle' => __( 'Specify default search position.', 'mpcth' ),
			'options'  => array(
				'left'  => __( 'Left', 'mpcth' ),
				'none'  => __( 'None', 'mpcth' ),
				'right' => __( 'Right', 'mpcth' ),
			),
			'default'  => 'none',
		),
		array(
			'id'       => 'sidebar__position-archive',
			'type'     => 'button_set',
			'title'    => __( 'Position - Archive', 'mpcth' ),
			'subtitle' => __( 'Specify default archive position.', 'mpcth' ),
			'options'  => array(
				'left'  => __( 'Left', 'mpcth' ),
				'none'  => __( 'None', 'mpcth' ),
				'right' => __( 'Right', 'mpcth' ),
			),
			'default'  => 'none',
		),

		//----------------------------------------------------------------------------//
		//	TYPOGRAPHY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'sidebar__font-start',
			'type'     => 'section',
			'title'    => __( 'Typography', 'mpcth' ),
			'subtitle' => __( 'Specify font setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'             => 'sidebar__font-title',
			'type'           => 'typography',
			'title'          => __( 'Title', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify title font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_sidebar .mpcth_widget__title', '.mpcth_main .widget .widgettitle' ),
		),
		array(
			'id'             => 'sidebar__font-content',
			'type'           => 'typography',
			'title'          => __( 'Content', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify content font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_sidebar .mpcth_widget__text', '.mpcth_main .widget', '.mpcth_sidebar a', '.mpcth_main .widget a', '.widget .sub-menu a' ),
		),
		array(
			'id'          => 'sidebar__font-color',
			'type'        => 'link_color',
			'title'       => __( 'Links - Colors', 'mpcth' ),
			'subtitle'    => __( 'Specify links colors.', 'mpcth' ),
			'active'      => false,
			'compiler'    => array( '.mpcth_sidebar a', '.mpcth_main .widget a', '.widget .sub-menu a' ),
		),
		array(
			'id'     => 'sidebar__font-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'sidebar__background-start',
			'type'     => 'section',
			'title'    => __( 'Background', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
		),
		array(
			'id'       => 'sidebar__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'sidebar__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'compiler' => array( '.mpcth_sidebar.mpcth_background--default' ),
			'required' => array( 'sidebar__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'sidebar__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_sidebar.mpcth_background--gradient' ),
			'required' => array( 'sidebar__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'sidebar__background-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
