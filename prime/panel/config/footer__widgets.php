<?php
/*----------------------------------------------------------------------------*/
/*--[ PANEL: FOOTER - WIDGETS ]--*/
/*----------------------------------------------------------------------------*/

Redux::setSection( MPC_THEME_OPTIONS, array(
	'title'      => __( 'Widgets', 'mpcth' ),
	'id'         => 'widgets__section',
	'desc'       => __( 'Settings for widgets.', 'mpcth' ),
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'widgets__disable',
			'type'     => 'switch',
			'title'    => __( 'Disable Widgets', 'mpcth' ),
			'subtitle' => __( 'Switch to disable widgets.', 'mpcth' ),
			'default'  => false,
		),

		array(
			'id'       => 'widgets__columns',
			'type'     => 'button_set',
			'title'    => __( 'Columns', 'mpcth' ),
			'subtitle' => __( 'Specify columns number in footer.', 'mpcth' ),
			'options'  => array(
				'1'     => __( 'One', 'mpcth' ),
				'2'     => __( 'Two', 'mpcth' ),
				'3'     => __( 'Three', 'mpcth' ),
				'4'     => __( 'Four', 'mpcth' ),
				'fluid' => __( 'Fluid', 'mpcth' ),
			),
			'default'  => '4',
			'required' => array( 'widgets__disable', '=', false ),
		),
		array(
			'id'       => 'widgets__align',
			'type'     => 'button_set',
			'title'    => __( 'Alignment', 'mpcth' ),
			'subtitle' => __( 'Specify columns alignment in footer.', 'mpcth' ),
			'options'  => array(
				'left'    => __( 'Left', 'mpcth' ),
				'center'  => __( 'Center', 'mpcth' ),
				'right'   => __( 'Right', 'mpcth' ),
			),
			'default'  => 'left',
			'required' => array( 'widgets__columns', '=', 'fluid' ),
		),
		array(
			'id'       => 'widgets__padding',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => array( 'px', 'em', '%' ),
			'title'    => __( 'Padding', 'mpcth' ),
			'subtitle' => __( 'Specify padding for footer.', 'mpcth' ),
			'compiler' => array( '.mpcth_widgets-wrapper' ),
			'required' => array( 'widgets__disable', '=', false ),
		),

		//----------------------------------------------------------------------------//
		//	TYPOGRAPHY
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'widgets__font-start',
			'type'     => 'section',
			'title'    => __( 'Typography', 'mpcth' ),
			'subtitle' => __( 'Specify font setting.', 'mpcth' ),
			'indent'   => true,
			'required' => array( 'widgets__disable', '=', false ),
		),
		array(
			'id'             => 'widgets__font-title',
			'type'           => 'typography',
			'title'          => __( 'Title', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify title font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_page__footer .mpcth_widget__title' ),
		),
		array(
			'id'             => 'widgets__font-content',
			'type'           => 'typography',
			'title'          => __( 'Content', 'mpcth' ),
			'units'          => 'px',
			'text-transform' => true,
			'subtitle'       => __( 'Specify content font settings.', 'mpcth' ),
			'compiler'       => array( '.mpcth_page__footer .mpcth_widget__text' ),
		),
		array(
			'id'       => 'widgets__font-links',
			'type'     => 'link_color',
			'title'    => __( 'Links - Colors', 'mpcth' ),
			'subtitle' => __( 'Specify links colors.', 'mpcth' ),
			'active'   => false,
			'compiler' => array( '.mpcth_page__footer .mpcth_widgets a' ),
		),
		array(
			'id'     => 'widgets__font-end',
			'type'   => 'section',
			'indent' => false,
		),

		//----------------------------------------------------------------------------//
		//	BACKGROUND
		//----------------------------------------------------------------------------//
		array(
			'id'       => 'widgets__background-start',
			'type'     => 'section',
			'title'    => __( 'Background', 'mpcth' ),
			'subtitle' => __( 'Specify background setting.', 'mpcth' ),
			'indent'   => true,
			'required' => array( 'widgets__disable', '=', false ),
		),
		array(
			'id'       => 'widgets__background-type',
			'type'     => 'select',
			'title'    => __( 'Type', 'mpcth' ),
			'subtitle' => __( 'Specify background type.', 'mpcth' ),
			'options'  => array(
				'default'  => __( 'Default', 'mpcth' ),
				'gradient' => __( 'Gradient', 'mpcth' ),
			),
			'default'  => 'default',
		),
		array(
			'id'       => 'widgets__background-default',
			'type'     => 'background',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'compiler' => array( '.mpcth_page__footer .mpcth_widgets-wrapper.mpcth_background--default' ),
			'required' => array( 'widgets__background-type', '=', 'default' ),
			'class'    => 'mpc-color-picker',
		),
		array(
			'id'       => 'widgets__background-gradient',
			'type'     => 'gradient',
			'title'    => __( 'Style', 'mpcth' ),
			'subtitle' => __( 'Specify background.', 'mpcth' ),
			'default'  => '#ffffff||#eeeeee||0;100||210||linear',
			'compiler' => array( '.mpcth_page__footer .mpcth_widgets-wrapper.mpcth_background--gradient' ),
			'required' => array( 'widgets__background-type', '=', 'gradient' ),
		),
		array(
			'id'     => 'widgets__background-end',
			'type'   => 'section',
			'indent' => false,
		),
	),
) );
