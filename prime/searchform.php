<?php
/*----------------------------------------------------------------------------*/
/*--[ THEME: SEARCH FORM ]--*/
/*----------------------------------------------------------------------------*/
?>
<form role="search" method="get" class="search-form mti-t-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<input type="search" class="search-field" placeholder="<?php echo esc_attr( __( 'Search ...', 'mpcth' ) ); ?>" value="<?php echo get_search_query(); ?>" name="s"/>
	<input type="submit" class="search-submit" value="<?php echo esc_attr( __( 'Search', 'mpcth' ) ); ?>"/>
</form>
