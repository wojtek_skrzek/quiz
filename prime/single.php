<?php
/*----------------------------------------------------------------------------*/
/*--[ THEME: SINGLE ]--*/
/*----------------------------------------------------------------------------*/

get_header();

	echo '<main id="mpcth_main" class="mpcth_main mpcth_single" role="main">';

	while ( have_posts() ) : the_post();

		get_template_part( 'template-parts/single', get_post_type() );

		if ( comments_open() || get_comments_number() ) {
			comments_template();
		}

	endwhile;

	echo '</main><!-- .mpcth_main -->';

get_sidebar();
get_footer();
