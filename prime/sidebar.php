<?php
/*----------------------------------------------------------------------------*/
/*--[ THEME: SIDEBAR ]--*/
/*----------------------------------------------------------------------------*/

global $mpc_theme;

if ( ! is_active_sidebar( 'sidebar-main' ) || mpcth_get_sidebar_position() == 'none' ) {
	return;
}

?>

<aside id="mpcth_sidebar" class="mpcth_sidebar mpcth_background--<?php echo $mpc_theme[ 'sidebar__background-type' ]; ?>" role="complementary">
	<?php dynamic_sidebar( 'sidebar-main' ); ?>
</aside><!-- #mpcth_sidebar -->
