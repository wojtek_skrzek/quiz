<?php
/*----------------------------------------------------------------------------*/
/*--[ MARKUP PARTS ]--*/
/*----------------------------------------------------------------------------*/

// DISPLAY SOCIAL ICONS
function mpcth_display_socials( $echo = false ) {
	global $mpc_theme;

	$socials_list = '';

	if ( ! empty( $mpc_theme[ 'socials__list' ] ) ) {
		foreach ( $mpc_theme[ 'socials__list' ] as $social ) {
			$socials_list .= '<a href="' . esc_url( $social[ 'url' ] ) . '" class="social ' . esc_attr( $social[ 'icon' ] ) . '"></a>';
		}

		if ( $echo ) {
			echo $socials_list;
		}

		return $socials_list;
	} else {
		return false;
	}
}

// DISPLAY LOGO
function mpcth_display_logo( $prefix = 'logo' ) {
	global $mpc_theme;

	if ( $mpc_theme[ $prefix . '__type' ] == 'image' && ! empty( $mpc_theme[ $prefix . '__image' ] ) ) {
		$alt_header = mpcth_get_metabox( '_mpcth_enable_alt_header', false );

		$logo = '<img class="mpcth_logo--def" src="' . $mpc_theme[ $prefix . '__image' ][ 'url' ] . '" width="' . $mpc_theme[ $prefix . '__image' ][ 'width' ] . '" height="' . $mpc_theme[ $prefix . '__image' ][ 'height' ] . '" />';

		if ( $alt_header ) {
			if ( ! empty( $mpc_theme[ $prefix . '__image--alt' ][ 'url' ] ) ) {
				$postfix = '--alt';
			} else {
				$postfix = '';
			}

			$logo .= '<img class="mpcth_logo--alt" src="' . $mpc_theme[ $prefix . '__image' . $postfix ][ 'url' ] . '" width="' . $mpc_theme[ $prefix . '__image' . $postfix ][ 'width' ] . '" height="' . $mpc_theme[ $prefix . '__image' . $postfix ][ 'height' ] . '" />';
		}
	} else {
		if ( $mpc_theme[ $prefix . '__text' ] == '' ) {
			$logo = get_bloginfo( 'name', 'display' );
		} else {
			$logo = $mpc_theme[ $prefix . '__text' ];
		}
	}

	return $logo;
}

// DISPLAY BREADCRUMBS
function mpcth_display_breadcrumb( $class = '' ) {
	global $mpc_theme, $post;

	$separator  = '<span class="mpcth_separator"></span>';
	$breadcrumb = '<span class="mpcth_current">' . get_the_title() . '</span>' . $separator;

	if ( $post->post_type == 'post' ) {
		$cats = get_the_category();

		if ( ! empty( $cats ) ) {
			$max_cat = $cats[ 0 ];

			if ( count( $cats ) > 1 ) {
				foreach ( $cats as $cat ) {
					if ( $max_cat->count < $cat->count ) {
						$max_cat = $cat;
					}
				}
			}

			$parents = get_category_parents( $max_cat->term_id, true, $separator );

			$breadcrumb = $parents . $breadcrumb;
		}

		$blog = get_option( 'page_for_posts' );

		if ( $blog == '0' ) {
			$blog = get_posts( array(
				'post_type' => 'page',
				'meta_key' => '_wp_page_template',
				'meta_value' => 'template-blog.php'
			) );

			if ( isset( $blog[ 0 ] ) ) {
				$blog = $blog[ 0 ];
			}
		} else {
			$blog = get_post( $blog );
		}

		if ( ! empty( $blog ) ) {
			$breadcrumb = '<a href="' . get_permalink( $blog->ID ) . '">' . $blog->post_title . '</a>' . $separator . $breadcrumb;

			mpcth_breadcrumb_parents( $blog, $breadcrumb, $separator );
		}
	} elseif( $post->post_type == 'mpc_portfolio' ) {
		$cats = get_the_terms( get_the_ID(), 'mpc_portfolio_cat' );

		if ( ! empty( $cats ) ) {
			$max_cat = $cats[ 0 ];

			if ( count( $cats ) > 1 ) {
				foreach ( $cats as $cat ) {
					if ( $max_cat->count < $cat->count ) {
						$max_cat = $cat;
					}
				}
			}

			$breadcrumb = '<a href="' . get_term_link( $max_cat->term_id, 'mpc_portfolio_cat' ) . '">' . $max_cat->name . '</a>' . $separator . $breadcrumb;

			if ( $max_cat->parent != 0 ) {
				$parent = get_term( $max_cat->parent, 'mpc_portfolio_cat' );
				while( ! is_wp_error( $parent ) ) {
					$breadcrumb = '<a href="' . get_term_link( $parent->term_id, 'mpc_portfolio_cat' ) . '">' . $parent->name . '</a>' . $separator . $breadcrumb;

					$parent = get_term( $parent->parent, 'mpc_portfolio_cat' );
				}
			}
		}

		$portfolio = get_posts( array(
			'post_type' => 'page',
			'meta_key' => '_wp_page_template',
			'meta_value' => 'template-portfolio.php'
		) );

		if ( isset( $portfolio[ 0 ] ) ) {
			$portfolio = $portfolio[ 0 ];
		}

		if ( ! empty( $portfolio ) ) {
			$breadcrumb = '<a href="' . get_permalink( $portfolio->ID ) . '">' . $portfolio->post_title . '</a>' . $separator . $breadcrumb;

			mpcth_breadcrumb_parents( $portfolio, $breadcrumb, $separator );
		}
	} elseif( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
		$class .= ' woocommerce-breadcrumb';

		ob_start();

		woocommerce_breadcrumb();

		$breadcrumb = ob_get_clean() . $separator;
	} elseif( $post->post_type == 'page' ) {
		mpcth_breadcrumb_parents( $post, $breadcrumb, $separator );
	}

	$home_url = get_home_url() . '/';

	if ( strpos( $breadcrumb, '"' . $home_url . '"' ) === false ) {
		$breadcrumb = '<a href="' . get_home_url() . '">' . __( 'Home', 'mpcth' ) . '</a>' . $separator . $breadcrumb;
	}

	$before = '';
	if ( isset( $mpc_theme[ 'breadcrumbs__before' ] ) && $mpc_theme[ 'breadcrumbs__before' ] != '' ) {
		$before = '<span class="mpcth_before">' . $mpc_theme[ 'breadcrumbs__before' ] . '</span>';
	}

	$after = '';
	if ( isset( $mpc_theme[ 'breadcrumbs__after' ] ) && $mpc_theme[ 'breadcrumbs__after' ] != '' ) {
		$after = '<span class="mpcth_after">' . $mpc_theme[ 'breadcrumbs__after' ] . '</span>';
	}

	$breadcrumb = '<nav class="mpcth_breadcrumbs ' . $class . '" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>' . $before . substr( $breadcrumb, 0, -strlen( $separator ) ) . $after . '</nav>';

	echo apply_filters( 'mpc_filter_breadcrumb', $breadcrumb );
}

// GET ALL PAGE PARENTS
function mpcth_breadcrumb_parents( $child, &$breadcrumb, $separator ) {
	if ( isset( $child->post_parent ) && $child->post_parent != 0 ) {
		$parent = get_post( $child->post_parent );

		$breadcrumb = '<a href="' . get_permalink( $parent->ID ) . '">' . $parent->post_title . '</a>' . $separator . $breadcrumb;

		mpcth_breadcrumb_parents( $parent, $breadcrumb, $separator );
	}
}