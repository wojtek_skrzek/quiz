<?php
/*----------------------------------------------------------------------------*/
/*--[ EXTRAS ]--*/
/*----------------------------------------------------------------------------*/

// BODY CLASSES
add_filter( 'body_class', 'mpcth_body_classes' );
function mpcth_body_classes( $classes ) {
	global $mpc_theme;

	$classes[] = 'mpcth_body';
	$classes[] = 'mpcth_display--' . $mpc_theme[ 'global__display' ];
	$classes[] = 'mpcth_sidebar--' . mpcth_get_sidebar_position();
	$classes[] = 'mpcth_footer--' . $mpc_theme[ 'footer__display' ];

	if ( $mpc_theme[ 'global__display' ] == 'boxed' ) {
		$classes[] = 'mpcth_background--' . $mpc_theme[ 'global__body-background-type' ];
	}

	return $classes;
}

// GET METABOX VALUE
function mpcth_get_metabox( $name, $default = false, $post_id = '' ) {
	if ( function_exists( 'get_field' ) ) {
		$value = get_field( $name, $post_id );

		if ( $value === null ) {
			return $default;
		} else {
			return $value;
		}
	} else {
		return $default;
	}
}

// GET SIDEBAR POSITION
function mpcth_get_sidebar_position() {
	global $page_id, $mpc_theme;

	$position  = 'none';
	$custom = mpcth_get_metabox( '_mpcth_sidebar_custom', false, $page_id );

	if ( $custom ) {
		$position = mpcth_get_metabox( '_mpcth_sidebar_position', 'none' );
	} elseif ( is_single() ) {
		$post_type = get_post_type( $page_id );

		if ( $post_type == 'post' )
			$position = $mpc_theme[ 'sidebar__position-post' ];
		elseif ( $post_type == 'mpc_portfolio' )
			$position = $mpc_theme[ 'sidebar__position-portfolio' ];
	} elseif ( function_exists( 'is_shop' ) && is_shop() ) {
		$position = $mpc_theme[ 'sidebar__position' ];
	} elseif ( is_search() ) {
		$position = $mpc_theme[ 'sidebar__position-search' ];
	} elseif ( is_archive() ) {
		$position = $mpc_theme[ 'sidebar__position-archive' ];
	} elseif ( is_page() || $page_id == 0 ) {
		$position = $mpc_theme[ 'sidebar__position' ];
	}

	if ( ! $position ) {
		$position = 'none';
	}

	return $position;
}

// GET VIDEO ID
function mpcth_parse_video_url( $url ) {
	preg_match( '/https?:\/\/(?:player.|www.)?(vimeo\.com|youtube\.com|youtu\.be)\/(?:video\/|embed\/|watch\?v=)?([A-Za-z0-9._%-]*)/', $url, $matches );

	if ( isset( $matches[ 0 ], $matches[ 1 ] ) ) {
		if ( $matches[ 0 ] === 'youtube.com' || $matches[ 0 ] === 'youtu.be' ) {
			return array( 'type' => 'youtube', 'id' => $matches[ 1 ] );
		} elseif ( $matches[ 0 ] === 'vimeo.com' ) {
			return array( 'type' => 'vimeo', 'id' => $matches[ 1 ] );
		} else {
			return array( 'type' => 'other', 'id' => $matches[ 1 ] );
		}
	}

	return false;
}