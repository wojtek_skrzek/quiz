<?php
/*----------------------------------------------------------------------------*/
/*--[ WOOCOMMERCE ]--*/
/*----------------------------------------------------------------------------*/

add_action( 'after_setup_theme', 'mpcth_woocommerce_setup' );
function mpcth_woocommerce_setup() {
	// WRAPPER
	add_action( 'woocommerce_before_main_content', 'mpcth_woocommerce_wrapper_start', 5 );
	add_action( 'woocommerce_after_main_content', 'mpcth_woocommerce_wrapper_end', 15 );
	add_action( 'woocommerce_before_shop_loop_item', 'mpcth_woocommerce_product_start', 5 );
	add_action( 'woocommerce_after_shop_loop_item', 'mpcth_woocommerce_product_end', 15 );

	// UNHOOK DEFAULT WOOCOMMERCE PARTS
	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

	// HOOK DEFAULT WOOCOMMERCE PARTS IN DIFFERENT PLACES
	add_action( 'woocommerce_archive_description', 'mpcth_display_breadcrumb', 5, 0 );
	add_action( 'woocommerce_single_product_summary', 'mpcth_display_breadcrumb', 4 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 15 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'mpcth_woocommerce_new_badge', 5 );
	add_action( 'woocommerce_before_single_product_summary', 'mpcth_woocommerce_new_badge', 5 );

	// FILTER DEFAULT WOOCOMMERCE PARTS
	add_filter( 'woocommerce_show_page_title', 'mpcth_woocommerce_title', 10 );
	add_filter( 'woocommerce_breadcrumb_defaults', 'mpcth_woocommerce_breadcrumb', 10 );
	add_filter( 'woocommerce_breadcrumb_home_url', 'mpcth_woocommerce_breadcrumb_url', 10 );
	add_filter( 'woocommerce_loop_add_to_cart_link', 'mpcth_woocommerce_add_to_cart', 100 );
	add_filter( 'loop_shop_per_page', 'mpcth_woocommerce_products_per_page', 20 );
	add_filter( 'single_product_archive_thumbnail_size', 'mpcth_woocommerce_thumbnail_size', 10 );

	// ADD SUPPORT FOR WOOCOMMERCE
	add_theme_support( 'woocommerce' );
}

add_filter( 'mpcth/css/file', 'mpcth_woocommerce_custom_styles_save' );
function mpcth_woocommerce_custom_styles_save( $css ) {
	global $mpc_theme;

	// SHOP
	if ( $mpc_theme[ 'wc_shop_product__rating-size' ] == 0 ) {
		$css .= '.mpcth_body.woocommerce .products .product .star-rating{display:none;}';
	} else {
		$css .= '.mpcth_body.woocommerce .products .product .star-rating{font-size:' . $mpc_theme[ 'wc_shop_product__rating-size' ] . 'px;}';
	}

	// SHOP GRID
	if ( $mpc_theme[ 'wc_shop__gap' ] != '0' ) {
		if ( $mpc_theme[ 'wc_shop__columns' ] != '1' ) {
			$css .= '.mpcth_body.woocommerce .mpcth_shop .product{padding-right:' . $mpc_theme[ 'wc_shop__gap' ] . 'px;padding-bottom:' . $mpc_theme[ 'wc_shop__gap' ] . 'px;}';
			$css .= '.mpcth_body.woocommerce .mpcth_shop .products{margin-right:-' . $mpc_theme[ 'wc_shop__gap' ] . 'px;}';
		} else {
			$css .= '.mpcth_body.woocommerce .mpcth_shop .product{margin-bottom:' . $mpc_theme[ 'wc_shop__gap' ] . 'px;}';
		}
	}

	// PRODUCT
	if ( $mpc_theme[ 'wc_product__rating-size' ] == 0 ) {
		$css .= '.mpcth_body.woocommerce.single-product .product .star-rating{display:none;}';
	} else {
		$css .= '.mpcth_body.woocommerce.single-product .product .star-rating{font-size:' . $mpc_theme[ 'wc_product__rating-size' ] . 'px;}';
	}

	$css .= '.mpcth_body.woocommerce .products .product{text-align:' . $mpc_theme[ 'wc_shop_product__align' ] . ';}';

	return $css;
}

// WRAPPER
function mpcth_woocommerce_wrapper_start() {
	global $mpc_theme;

	$classes = '';
	if ( is_shop() ) {
		$classes = ' mpcth_shop mpcth_columns--' . $mpc_theme[ 'wc_shop__columns' ];
	}

	echo '<main id="mpcth_main" class="mpcth_main' . $classes . '" role="main">';
}

function mpcth_woocommerce_wrapper_end() {
	echo '</main>';
}

function mpcth_woocommerce_product_start() {
	echo '<div class="mpcth_product-wrapper">';
}

function mpcth_woocommerce_product_end() {
	echo '</div>';
}

// DISABLE TITLE
function mpcth_woocommerce_title() {
	return false;
}

function mpcth_woocommerce_breadcrumb( $args ) {
	$args[ 'delimiter' ]   = '<span class="mpcth_separator"></span>';
	$args[ 'wrap_before' ] = '';
	$args[ 'wrap_after' ]  = '';

	if ( is_shop() ) {
		$args[ 'home' ] = false;
	} else {
		$args[ 'home' ] = __( 'Shop', 'mpcth' );
	}

	return $args;
}

function mpcth_woocommerce_breadcrumb_url( $url ) {
	return get_permalink( wc_get_page_id( 'shop' ) );
}

function mpcth_woocommerce_add_to_cart( $markup ) {
	return '<div class="mpcth_add-to-cart">' . $markup . '</div>';
}

function mpcth_woocommerce_new_badge() {
	global $post, $product, $mpc_theme;

	$new_badge_time = ( $mpc_theme[ 'wc_badge__new-days' ] ?: 30 ) * DAY_IN_SECONDS;
	$current_time = time();
	$product_time = strtotime( $product->post->post_date );

	if ( $product_time > $current_time - $new_badge_time ) {
		echo '<span class="mpcth_badge--new">' . __( 'New', 'mpcth' ) . '</span>';
	}
}

function mpcth_woocommerce_products_per_page() {
	global $mpc_theme;

	return $mpc_theme[ 'wc_shop__posts-num' ];
}

function mpcth_woocommerce_thumbnail_size() {
	return 'mpcth-thumb--large';
}

//----------------------------------------------------------------------------//
//	MINI CART
//----------------------------------------------------------------------------//
add_action( 'mpcth/header/elements', 'mpcth_woocommerce_display_cart' );
function mpcth_woocommerce_display_cart() {
	global $mpc_theme;

	if ( ! $mpc_theme[ 'header__disable-cart' ] ) : ?>
		<div id="mpcth_mini_cart" class="mpcth_mini-cart">
			<div id="mpcth_cart_info" class="mpcth_cart-info"></div>
			<div id="mpcth_cart" class="mpcth_cart"></div>
		</div>
	<?php endif;
}

// MINI CART ICON
add_filter( 'add_to_cart_fragments', 'mpcth_woocommerce_cart_icon' );
function mpcth_woocommerce_cart_icon( $fragments ) {
	global $mpc_theme;

	$count = WC()->cart->get_cart_contents_count();

	$result = '<div id="mpcth_cart_info" class="mpcth_cart-info' . ( $count == 0 ? ' mpcth_empty' : '' ) . '">';
		if ( ! $mpc_theme[ 'wc_cart__disable-price' ] ) {
			$result .= '<span class="mpcth_cart-price">' . WC()->cart->get_cart_subtotal() . '</span>';
		}
		$result .= '<span class="mpcth_cart-icon mti-ei-bag"></span>';
		if ( ! $mpc_theme[ 'wc_cart__disable-counter' ] ) {
			$result .= '<span class="mpcth_cart-count">' . $count . '</span>';
		}
	$result .= '</div>';

	$fragments[ '#mpcth_cart_info' ] = $result;

	return $fragments;
}

// MINI CART LIST
add_filter( 'add_to_cart_fragments', 'mpcth_woocommerce_cart_list' );
function mpcth_woocommerce_cart_list( $fragments ) {
	global $mpc_theme;

	$classes = WC()->cart->get_cart_contents_count() == 0 ? ' mpcth_empty' : '';
	$classes .= ' mpcth_background--' . $mpc_theme[ 'wc_cart__background-type' ];

	ob_start();

	echo '<div id="mpcth_cart" class="mpcth_cart' . $classes . '">';
		mpcth_woocommerce_display_cart_list();
	echo '</div>';

	$fragments[ '#mpcth_cart' ] = ob_get_clean();

	return $fragments;
}

function mpcth_woocommerce_display_cart_list() { ?>
	<?php if ( ! WC()->cart->is_empty() ) : ?>

		<ul class="mpcth_cart-list">

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) :
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item[ 'data' ], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item[ 'product_id' ], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item[ 'quantity' ] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) :

					$product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
					$thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
					$product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					$wrapper_end       = '';
					?>
					<li class="<?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mpcth_cart-item', $cart_item, $cart_item_key ) ); ?>">
						<?php if ( ! $_product->is_visible() ) : ?>
							<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ) . $product_name; ?>
						<?php else : ?>
							<?php $wrapper_end = '</div>'; ?>
							<a href="<?php echo esc_url( $product_permalink ); ?>" class="mpcth_image">
								<?php echo str_replace( array( 'http:', 'https:' ), '', $thumbnail ); ?>
							</a>
							<div class="mpcth_desc">
								<a href="<?php echo esc_url( $_product->get_permalink( $cart_item ) ); ?>" class="mpcth_title">
									<?php echo '<h5>' . $product_name . '</h5>'; ?>
								</a>
								<?php echo '<span class="mpcth_price">' . __( 'Price: ', 'mpcth' ) . $product_price . '</span>'; ?>
								<?php echo '<span class="mpcth_quantity">' . __( 'Quantity: ', 'mpcth' ) . $cart_item[ 'quantity' ] . '</span>'; ?>
						<?php endif; ?>
						<?php echo WC()->cart->get_item_data( $cart_item ); ?>

						<?php
						echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
							'<a href="%s" class="mpcth_remove" title="%s" data-product_id="%s" data-product_sku="%s">' . __( 'Remove', 'mpcth' ) . '</a>',
							esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
							__( 'Remove this item', 'mpcth' ),
							esc_attr( $product_id ),
							esc_attr( $_product->get_sku() )
						), $cart_item_key );
						?>

						<?php echo $wrapper_end; ?>
					</li>
					<?php
				endif;
			endforeach;
			?>

		</ul><!-- end product list -->

		<p class="total">
			<strong><?php _e( 'Cart Subtotal', 'mpcth' ); ?>:</strong> <?php echo WC()->cart->get_cart_subtotal(); ?>
		</p>

		<?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>

		<p class="buttons">
			<a href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="button"><?php _e( 'View Cart', 'mpcth' ); ?></a>
			<a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="button alt"><?php _e( 'Proceed to Checkout', 'mpcth' ); ?></a>
		</p>

	<?php else : ?>

		<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>" class="button mpcth_button--shop"><?php _e( 'Shop Now', 'mpcth' ); ?></a>
		<p class="empty"><?php _e( 'You have no products in the cart.', 'mpcth' ); ?></p>

	<?php endif;
}