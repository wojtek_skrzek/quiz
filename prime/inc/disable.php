<?php
/*----------------------------------------------------------------------------*/
/*--[ WORDPRESS VERSION ]--*/
/*----------------------------------------------------------------------------*/

add_action( 'after_switch_theme', 'mpcth_disable_theme' );
function mpcth_disable_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );

	unset( $_GET['activated'] );

	add_action( 'admin_notices', 'mpcth_upgrade_wordpress_notice' );
}

function mpcth_upgrade_wordpress_notice() {
	printf( '<div class="error"><p>%s</p></div>', __( 'MPC Themes require WordPress version 4.4 or newer. Please upgrade and try again.', 'mpcth' ) );
}

add_action( 'load-customize.php', 'mpcth_disable_customize' );
function mpcth_disable_customize() {
	wp_die( __( 'MPC Themes require WordPress version 4.4 or newer. Please upgrade and try again.', 'mpcth' ), '', array( 'back_link' => true, ) );
}

add_action( 'template_redirect', 'mpcth_disable_preview' );
function mpcth_disable_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( __( 'MPC Themes require WordPress version 4.4 or newer. Please upgrade and try again.', 'mpcth' ) );
	}
}
