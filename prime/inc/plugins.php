<?php
/*----------------------------------------------------------------------------*/
/*--[ PLUGINS ACTIVATION ]--*/
/*----------------------------------------------------------------------------*/

require_once MPC_THEME_DIR . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'mpcth_register_required_plugins' );
function mpcth_register_required_plugins() {
	$plugins = array(
		array(
			'name'               => 'Advanced Custom Fields PRO',
			'slug'               => 'advanced-custom-fields-pro',
			'source'             => MPC_THEME_DIR . '/plugins/advanced-custom-fields-pro.zip',
			'required'           => true,
			'version'            => '5.4.2 ',
			'force_activation'   => true,
			'force_deactivation' => false,
			'external_url'       => '',
			'is_callable'        => '',
		),
//		array(
//			'name'      => 'BuddyPress',
//			'slug'      => 'buddypress',
//			'required'  => false,
//		),
	);

	$config = array(
		'id'           => 'mpcth',
		'default_path' => '',
		'menu'         => 'tgmpa-install-plugins',
		'has_notices'  => true,
		'dismissable'  => false,
		'dismiss_msg'  => '',
		'is_automatic' => true,
		'message'      => '',
	);

	tgmpa( $plugins, $config );
}
