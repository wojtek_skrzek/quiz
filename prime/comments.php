<?php
/*----------------------------------------------------------------------------*/
/*--[ THEME: COMMENTS ]--*/
/*----------------------------------------------------------------------------*/

if ( post_password_required() ) {
	return;
}

?>

<div id="mpcth_comments" class="mpcth_comments">

	<?php if ( have_comments() ) : ?>

		<h2 class="mpcth_comments__title"><?php _e( 'Comments', 'mpcth' ); ?></h2>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav class="mpcth_comments__nav mpcth_top" role="navigation">
				<div class="mpcth_prev"><?php previous_comments_link( esc_html__( 'Older Comments', 'mpcth' ) ); ?></div>
				<div class="mpcth_next"><?php next_comments_link( esc_html__( 'Newer Comments', 'mpcth' ) ); ?></div>
			</nav>
		<?php endif; ?>

		<ul class="mpcth_comments__list">
			<?php
				wp_list_comments( array(
					'style'       => 'ul',
					'short_ping'  => true,
					'avatar_size' => 40,
				) );
			?>
		</ul>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
			<nav class="mpcth_comments__nav mpcth_bottom" role="navigation">
				<div class="mpcth_prev"><?php previous_comments_link( esc_html__( 'Older Comments', 'mpcth' ) ); ?></div>
				<div class="mpcth_next"><?php next_comments_link( esc_html__( 'Newer Comments', 'mpcth' ) ); ?></div>
			</nav>
		<?php endif; ?>

	<?php endif; ?>

	<?php if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
		<p class="mpcth_comments--closed"><?php esc_html_e( 'Comments are closed.', 'mpcth' ); ?></p>
	<?php endif; ?>

	<?php comment_form(); ?>

</div><!-- #mpcth_comments -->
