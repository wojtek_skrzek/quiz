<?php
/*----------------------------------------------------------------------------*/
/*--[ THEME: FOOTER ]--*/
/*----------------------------------------------------------------------------*/

global $mpc_theme;

// CHECK PARTS
$display_widgets    = ! $mpc_theme[ 'widgets__disable' ];
$display_copyrights = ! $mpc_theme[ 'copyrights__disable' ] && ( $mpc_theme[ 'copyrights__content-align' ] != 'none' || $mpc_theme[ 'copyrights__socials-align' ] != 'none' );

if ( $display_widgets ) {
	if ( $mpc_theme[ 'widgets__columns' ] == 'fluid' ) {
		$display_widgets = $display_widgets && is_active_sidebar( 'footer-main' );
	} else {
		$display_widgets = $display_widgets && ( is_active_sidebar( 'footer-main-1' ) || is_active_sidebar( 'footer-main-2' ) || is_active_sidebar( 'footer-main-3' ) || is_active_sidebar( 'footer-main-4' ) );
	}
}

// CLASSES
$back_to_top_classes = ' mpcth_background--' . $mpc_theme[ 'to_top__background-type' ];
$back_to_top_classes .= ' mpcth_position--' . $mpc_theme[ 'to_top__position' ];

$footer_classes = ' mpcth_background--' .  $mpc_theme[ 'footer__background-type' ];
$footer_classes .= ' mpcth_style--' .  $mpc_theme[ 'footer__display' ];

$columns_classes = ' mpcth_columns--' . $mpc_theme[ 'widgets__columns' ];
$columns_classes .= $mpc_theme[ 'widgets__columns' ] == 'fluid' ? ' mpcth_side--' . $mpc_theme[ 'widgets__align' ] : '';

$copyrights_classes = ! $mpc_theme[ 'copyrights__stretch' ] ? ' mpcth_layout--block' : '';

// COPYRIGHTS MARKUP
if ( $display_copyrights ) {
	$text    = '<div class="mpcth_text">' . $mpc_theme[ 'copyrights__content' ] . '</div>';
	$socials = '<div class="mpcth_socials">' . mpcth_display_socials() . '</div>';

	$copyrights_markup = '';
	if ( $mpc_theme[ 'copyrights__content-align' ] == $mpc_theme[ 'copyrights__socials-align' ] ) {
		$copyrights_markup = '<div class="mpcth_block mpcth_align--' . $mpc_theme[ 'copyrights__content-align' ] . '">' . $text . $socials . '</div>';
	} else {
		if ( $mpc_theme[ 'copyrights__content-align' ] == 'none' || $mpc_theme[ 'copyrights__socials-align' ] == 'none' ) {
			$copyrights_classes .= ' mpcth_auto';
		}

		foreach ( array( 'left', 'center', 'right' ) as $position ) {
			$copyrights_markup .= '<div class="mpcth_block mpcth_align--' . $position . '">';
				if ( $mpc_theme[ 'copyrights__content-align' ] == $position ) { $copyrights_markup .= $text; }
				if ( $mpc_theme[ 'copyrights__socials-align' ] == $position ) { $copyrights_markup .= $socials; }
			$copyrights_markup .= '</div>';;
		}
	}
}

?>

			<?php do_action( 'mpcth/content/end' ); ?>

		</div><!-- .mpcth_layout--block -->
	</div><!-- #mpcth_page_content -->

	<?php if ( ! $mpc_theme[ 'to_top__disable' ] ) : ?>
		<a href="#mpcth_back_to_top__anchor" id="mpcth_back_to_top" class="mpcth_back-to-top <?php echo $back_to_top_classes; ?>"><i class="mpcth_icon mti-fa-angle-up"></i></a>
	<?php endif; ?>

	<footer id="mpcth_page_footer" class="mpcth_page__footer<?php echo $footer_classes; ?>" role="contentinfo">

		<?php do_action( 'mpcth/footer/begin' ); ?>

		<?php if ( $display_widgets ) : ?>
			<div class="mpcth_widgets-wrapper mpcth_background--<?php echo $mpc_theme[ 'widgets__background-type' ]; ?>">
				<div class="mpcth_widgets mpcth_layout--block">
					<div class="mpcth_columns-wrapper<?php echo $columns_classes; ?>">
						<?php
						if ( $mpc_theme[ 'widgets__columns' ] == 'fluid' ) {
							dynamic_sidebar( 'footer-main' );
						} else {
							$columns = intval( $mpc_theme[ 'widgets__columns' ] ) ?: 4;

							for ( $i = 1; $i <= $columns; $i++ ) {
								echo '<div class="mpcth_column">';
								    dynamic_sidebar( 'footer-main-' . $i );
								echo '</div>';
							}
						}
						?>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<?php if ( $display_copyrights ) : ?>
			<div class="mpcth_copyrights-wrapper mpcth_background--<?php echo $mpc_theme[ 'copyrights__background-type' ]; ?>">
				<div class="mpcth_copyrights<?php echo $copyrights_classes; ?>">
					<?php echo $copyrights_markup; ?>
				</div>
			</div>
		<?php endif; ?>

		<?php do_action( 'mpcth/footer/end' ); ?>

	</footer><!-- #mpcth_page_footer -->

	<?php do_action( 'mpcth/site/end' ); ?>

</div><!-- #mpcth_page -->

<?php wp_footer(); ?>

<?php if ( trim( $mpc_theme[ 'js_editor' ] ) != '' ) : ?>
	<script><?php echo trim( $mpc_theme[ 'js_editor' ] ); ?></script>
<?php endif; ?>

<?php do_action( 'mpcth/body/end' ); ?>

</body>
</html>
